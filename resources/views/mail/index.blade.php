<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <h3>{{ $details['title'] }}</h3>
    <p>{{ $details['body'] }}</p>
    @if($details['link'] != '')
    <a href="{{ $details['link'] }}" style="padding: .7em;border: 0;border-radius: .3em;background: rgba(54, 153, 255);color: white;font-weight: 600px;margin-top: 4em;text-decoration: none;">{{$details['btn_label']}}</a>
    @endif
    <p>Thank you</p>
</body>
</html>