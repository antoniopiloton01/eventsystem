@extends('layouts.main')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Dashboard
    </h3>
</div>
<div class="row mb-4">
    <div class="col-md-3">
        <div class="stretch-card grid-margin">
            <div class="card bg-gradient-danger card-img-holder text-white">
                <div class="card-body p-4">
                    <img src="{{ asset('assets/images/dashboard/circle.svg') }}" class="card-img-absolute"
                        alt="circle-image" />
                    <h4 class="font-weight-normal">Events <i
                            class="mdi mdi-chart-line mdi-24px float-right"></i>
                    </h4>
                    <h1>{{$events}}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="stretch-card grid-margin">
            <div class="card bg-gradient-info card-img-holder text-white">
                <div class="card-body p-4">
                    <img src="{{ asset('assets/images/dashboard/circle.svg') }}" class="card-img-absolute"
                        alt="circle-image" />
                    <h4 class="font-weight-normal">Verified Users <i
                            class="mdi mdi-chart-line mdi-24px float-right"></i>
                    </h4>
                    <h1>{{$members}}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="stretch-card grid-margin">
            <div class="card bg-gradient-info card-img-holder text-white">
                <div class="card-body p-4">
                    <img src="{{ asset('assets/images/dashboard/circle.svg') }}" class="card-img-absolute"
                        alt="circle-image" />
                    <h4 class="font-weight-normal">Accomplished <i
                            class="mdi mdi-chart-line mdi-24px float-right"></i>
                    </h4>
                    <h1>{{$accomplished_event}}</h1>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Upcoming Events</div>
                <div id="upcoming"></div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
setInterval(function(){
    upcoming()
},1000)
upcoming()
function upcoming() {
    $.ajax({
        type: "GET",
        url: "{{route('x_upcoming')}}",
        // dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            $("#upcoming").html(data)
        },
        error: function(data) {
            toastr.show(data.message)
        }
    });
}

</script>
@endsection