@include('layouts.partials.styles')
<body>
    <div class="container-scroller">
        @include('layouts.partials.headers')
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel" style="width: 100% !important;">
                <div class="content-wrapper">
                    @yield('content')
                </div>
                @include('layouts.partials.footer')
            </div>
        </div>
    </div>
</body>
@include('layouts.partials.scripts')