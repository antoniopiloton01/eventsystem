@include('layouts.partials.styles')
<body>
    <div class="container-scroller">
        @include('layouts.partials.headers')
        <div class="container-fluid page-body-wrapper">
            @include('layouts.partials.sidebar')
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content')
                </div>
                @include('layouts.partials.footer')
            </div>
        </div>
    </div>
</body>
@include('layouts.partials.scripts')

