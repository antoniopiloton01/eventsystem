<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row shad">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center bg-white-mobile red-theme">
        <a class="navbar-brand brand-logo font-weight-bold systemTitle" href="/">Event Management</a>
        <a class="navbar-brand brand-logo-mini ml-5 font-weight-bold" href="#">
        <img src="{{ asset('images/noname.png') }}"  style="width:40px !important"> USA
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
        </div>

        <ul class="navbar-nav navbar-nav-right">
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>