<footer class="footer p-3 bg-white">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">University of San Agustin</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> <a href="https://www.usa.edu.ph">  All Rights Reserved - &copy; Copyright 2021 </a></span>
  </div>
</footer>
