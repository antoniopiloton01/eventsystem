<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=5.0, minimum-scale=1"> -->
    <title>USA Event with QR Code</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/cooltipz-css@2.1.0/cooltipz.css" />
    <link href="{{ asset('theme/assets/vendors/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/images/L.png') }}" rel="shortcut icon">
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-icons/bootstrap-icons.css') }}">
    <link href="{{ asset('theme/assets/css/classroom.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/css/bootstrap-duallistbox.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}"
        rel="stylesheet">
    <link href="{{ asset('theme/vendors/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/vendors/fullcalendar/dist/fullcalendar.print.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/DataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/DataTables/Buttons-2.0.0/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/DataTables/Buttons-2.0.0/css/buttons.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/DataTables/Buttons-2.0.0/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/DataTables/Buttons-2.0.0/css/buttons.bulma.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dist/plugins/jquery-toastr/jquery.toast.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/croppie.min.css')}}" rel="stylesheet">
    <link href="{{ asset('theme/assets/js/toaster/toastr.css')}}" rel="stylesheet">
    <link href="{{ asset('theme/assets/js/toaster/toastr.min.css')}}" rel="stylesheet">
    <link href="{{ asset('theme/assets/css/daterangepicker.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom2.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <meta name="theme-color" content="#ffff">
    <!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#ffff">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
</head>
<body data-theme="blue" id="QWererw">
<div id="loader" style="width: 40px; height: 40px;" role="progressbar"
    class="modal-dialog-centered MuiCircularProgress-root MuiCircularProgress-colorPrimary MuiCircularProgress-indeterminate">
    <svg viewBox="22 22 44 44" class="MuiCircularProgress-svg">
        <circle cx="44" cy="44" r="20.2" fill="none" stroke-width="3.6"
            class="MuiCircularProgress-circle MuiCircularProgress-circleIndeterminate"></circle>
    </svg>
</div>