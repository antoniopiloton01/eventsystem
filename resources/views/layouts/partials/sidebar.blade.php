<nav class="sidebar sidebar-offcanvas shadow" id="sidebar">
    <ul class="nav">
        <li class="nav-item text-center" style="text-align:center !important;">
            <div class="nav-profile-image nav-item mt-5">
                <img src="{{asset('images/noname.png')}}" alt=""
                    style="width: 100px;height: 100px;">
            </div>
            <a class="nav-link" data-toggle="collapse" href="#ui-profile" aria-expanded="false"
                aria-controls="ui-profile">
                <div class="nav-profile-text d-flex flex-column">
                    <span class="font-weight-bold mb-2">{{Auth::user()->name}} <i
                            class="mdi mdi-bookmark-check text-success nav-profile-badge"
                            style="transform: translateY(5px);"></i></span>
                    <span class="text-secondary text-small">
                        {{Auth::user()->email}}
                    </span>
                </div>
                <i class="mdi mdi-chevron-down"></i>
            </a>
            <div class="collapse" id="ui-profile">
                <ul class="nav flex-column sub-menu">
                    <!-- <li class="nav-item"> <a class="nav-link" href="">Profile</a></li>
                    <li class="nav-item"> <a class="nav-link" href="">Change Password </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </li>

        @if(isset($permissions['user_manage']))
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-enrollment" aria-expanded="false"
                aria-controls="ui-enrollment">
                <span class="menu-title">User Management</span>
                <i class="mdi mdi-contacts menu-icon"></i>
            </a>
            <div class="collapse" id="ui-enrollment">
                <ul class="nav flex-column sub-menu">
                    @if(isset($permissions['user_list']))
                    <li class="nav-item"> <a class="nav-link" href="{{route('users')}}">Masterlist</a></li>
                    @endif
                    @if(isset($permissions['user_add']))
                    <li class="nav-item"> <a class="nav-link" href="{{ route('addUsers') }}">Add New User</a></li>
                    @endif
                </ul>
            </div>
        </li>
        @endif
        @if(isset($permissions['event_manage']))
        @endif
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-event" aria-expanded="false"
                aria-controls="ui-enrollment">
                <span class="menu-title">Event Management</span>
                <i class="mdi mdi-contacts menu-icon"></i>
            </a>
            <div class="collapse" id="ui-event">
                <ul class="nav flex-column sub-menu">
                    @if(isset($permissions['event_list']))
                    @endif
                    <li class="nav-item"> <a class="nav-link" href="{{route('events')}}">Event</a></li>
                    
                    @if(isset($permissions['event_add']))
                    @endif
                    <li class="nav-item"> <a class="nav-link" href="{{route('event_add')}}">Add New Event</a></li>
                </ul>
            </div>
        </li>
        
    </ul>
</nav>