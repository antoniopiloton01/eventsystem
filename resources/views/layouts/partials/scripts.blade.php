<!-- jQuery  -->
<script type="text/javascript">
var baseUrl = '{{url(' / ')}}';
var dev = false;
@if(env('APP_ENV') == 'development')
dev = true;
@endif
</script>
<!-- jQuery -->
<!-- <script src="{{ asset('dist/assets/js/bootstrap.min.js')}}"></script> -->
<script src="{{ asset('theme/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('theme/assets/vendors/js/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/js/off-canvas.js') }}"></script>
<script src="{{ asset('theme/assets/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('theme/assets/js/misc.js') }}"></script>
<script src="{{ asset('theme/assets/js/todolist.js') }}"></script>
<script src="{{ asset('js/croppie.min.js') }} "></script>
<script src="{{ asset('theme/assets/js/custom.js') }}"></script>

<script src="{{ asset('dist/assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('dist/assets/js/jquery.slimscroll.js')}}"></script>

<script src="{{ asset('dist/assets/js/jquery.core.js')}}"></script>
<script src="{{ asset('dist/assets/js/jquery.app.js')}}"></script>

<script src="{{ asset('theme/vendors/select2/dist/js/select2.min.js') }}"></script>

<script src="{{ asset('theme/assets/js/jquery.bootstrap-duallistbox.js') }}"></script>
<script src="{{ asset('theme/vendors/gauge.js/dist/gauge.min.js') }}"></script>
<script src="{{ asset('theme/vendors/DateJS/build/date.js') }}"></script>

<script src="{{ asset('theme/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('theme/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('theme/vendors/jquery-ui/jquery-ui.js') }}"></script>

<script src="{{ asset('theme/vendors/fullcalendar/dist/fullcalendar.js') }}"></script>


<script type="text/javascript" src="{{ asset('theme/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('theme/vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('theme/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('theme/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('theme/assets/js/datatable.bundle.js') }}"></script>


<script src="{{ asset('theme/assets/js/modal.js')}}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/costum2.js') }} "></script>
<script src="{{ asset('theme/assets/js/custom.js') }} "></script>

<script src="{{ asset('theme/assets/js/sweet.js')}}"></script>
<!-- <script src="{{ asset('theme/assets/js/toaster/toastr.js')}}"></script> -->
<script src="{{ asset('theme/assets/js/toaster/toastr.min.js')}}"></script>
<script src="{{ asset('theme/assets/js/chart.js')}}"></script>
<script src="{{ asset('theme/assets/js/daterange.js')}}"></script>

<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script type="text/javascript">
var toastr = new Toastr({
    animation: 'slide',
    autohide: true,
    position: 'topCenter'
});
</script>

@yield('scripts')