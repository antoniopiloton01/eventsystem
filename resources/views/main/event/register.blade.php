@extends('layouts.register')
@section('content')
@if(isset($data))
<div class="page-header">
    <h3 class="page-title col-md-8 offset-md-2">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Register here
    </h3>
</div>

<div class="row mt-4 datas">
    <div class="col-md-8 offset-md-2">
        <div class="car">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 card p-3">
                        <label>Event Name</label>
                        <h3 class="text-primary">{{$data['event_name']}}</h3>
                    </div>
                    <div class="col-md-4 card p-3">
                        <label>Event Schedule(s)</label>
                        <h5>
                            @foreach($sched as $k)
                            <div class="d-block mt-2 h7">{{date('M. d, Y',strtotime($k->date))}}<span class="text-primary d-block mt-1">{{date('h:i:s A',strtotime($k->in))}} <i class="mdi mdi-arrow-right"></i> {{date('h:i:s A',strtotime($k->out))}}</span></div>
                            @endforeach
                        </h5>
                    </div>
                    <div class="col-md-4 card p-3">
                        <label>Event Setting</label>
                        <h5 class="text-success d-block">{{ $data['event_setting'] }} {{ $data['platform'] }}</h5>
                    </div>
                    <div class="col-md-4 card p-3">
                        <label>Start of Registration</label>
                        <h5 class="text-success d-block">{{ date('M. d, Y h:i:s A',strtotime($data['event_startReg'])) }}</h5>

                        <label>Deadline of Registration</label>
                        <h5 class="text-success d-block">{{ date('M. d, Y h:i:s A',strtotime($data['event_deadline'])) }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body data">
                @if($data['status'] != 'done')
                    @if($data['event_deadline'] >= date('Y-m-d H:i:s'))
                    <form  id="memberForm" onsubmit="saveMember(this)" enctype="multipart/form-data" data-parsley-validate=""
                    class="form-horizontal form-label-left row" novalidate="">
                    {{ csrf_field() }}
                        <div class="form-group col-md-6">
                            <label>Name</label>
                            <input type="hidden" name="code" placeholder="Name" class="form-control" value="{{encrypt($code)}}">
                            <input type="text" name="name" placeholder="Name" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>University Email</label>
                            <input type="email" name="email" placeholder="University Email" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Cellphone Number</label>
                            <input type="text" name="cp" placeholder="w/o Country Code" maxlength="11" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Department</label>
                            <input type="text" name="department" placeholder="" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Course</label>
                            <input type="text" name="course" placeholder="" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Year/Section</label>
                            <input type="text" name="yearsection" placeholder=""  class="form-control">
                        </div>
                        <div class="form-group col-md-6 offset-md-3">
                            <button class="btn btn-primary btn-lg btn-block">Register</button>
                        </div>
                    </form>
                    @endif
                @else
                <div class="text-center text-success"><h1><i class="mdi mdi-check-circle"></i></h1><h4>ACCOMPLISHED EVENT</h4></div>
                @endif
            </div>
        </div>
    </div>
</div>
@else
<div class="row mt-4">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-body text-center text-danger">
                <h4>EVENT CODE NOT FOUND</h4>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@section('scripts')
<script type="text/javascript">
$('#memberForm').on('submit', function(e) {
    e.preventDefault();
});
// setInterval(function(){
//     var today = new Date();
//     var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
//     var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//     var dateTime = date+' '+time;

//     console.log(dateTime +'>='+ '{{$data["event_deadline"]}}')

//     if(dateTime >= '{{$data["event_deadline"]}}'){
//         $(".data").html('<h1>{{$data["event_name"]}}</h1> <br>The form {{$data["event_name"]}} is no longer accepting applicant. <br>Try contacting the owner of the form if you think that this is a mistake.')
//         $(".datas").hide()
//         $(".page-title").hide()
//     }else if(dateTime >= '{{$data["event_startReg"]}}'){
//         $(".data").html('<h1>{{$data["event_name"]}}</h1> <br>The form {{$data["event_name"]}} is no longer accepting applicant. <br>Try contacting the owner of the form if you think that this is a mistake.')
//         $(".datas").hide()
//         $(".page-title").hide()
//     }
// })

function saveMember(formData) {
    loaderIn()
    var url = "{{ route('x_saveMember')}}";
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(formData),
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            Swal.fire({
                title: data.title,
                html:  data.message,
                icon: data.icon
            });
            loaderOut()
        },
        error: function(data) {
            message = 'We are unable to process request';
            if (data.responseJSON !== undefined) {
                message = '';
                for (var i in data.responseJSON.errors) {
                    var d = data.responseJSON.errors[i];
                    message += d + '<br>';
                }
            }
            Swal.fire({
                title: 'Error',
                html: message,
                icon: 'error'
            });
            loaderOut()
        }
    });
}
</script>
@endsection