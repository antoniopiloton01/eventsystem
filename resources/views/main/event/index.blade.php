@extends('layouts.main')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Events Masterlist
    </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered " id="events" width="100%" cellspacing="0">
                    <thead>
                        <th width="10%">****</th>
                        <th>Event</th>
                        <th>Date Time</th>
                        <th>Setting</th>
                        <th>Participants</th>
                        <th>***</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
var tab = $('#events').dataTable({
  processing: true,
  serverSide: true,
  paging: true,
  ajax: "{{route('x_events')}}",
  columnDefs: [{
    "targets": [1,2,3,4,5],
    "orderable": true,
    "searchable": true,
  }, {
    "targets": [1,2,3,4,5],
    "orderable": false,
    "searchable": false,
  },
   {
      "targets": [1,2,3,4,5],
  },
  ],
  bJQueryUI: false,
  bAutoWidth: true,
  sPaginationType: "full_numbers",
  dom: 'Bfrtip',
//   responsive: true,
  lengthMenu: [
      [ 10, 50, 100, 500, 1000, 2000, 3000, 5000, 100000],
      [ '10 rows', '50 rows', '100 rows', '500 rows', '1000 rows', '2000 rows', '3000 rows', '5000 rows', 'Show all' ]
  ],
  buttons: [
      'pageLength',
      {
      extend: 'copy',
      text: 'Copy to clipboard'
  },
  {
    extend: 'excel',
    download: 'open'
            },
  {
    extend: 'pdfHtml5',
    download: 'open'
            }
  ]
});
$(".dataTables_length select").select2({
  minimumResultsForSearch: "-5"
});

function delEvent(a){
    var formData = new FormData();
    formData.append('event_code',a)
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        loaderIn()
        var url = "{{ route('x_delEvent')}}";
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                Swal.fire({
                    title: data.title,
                    html:  data.message,
                    icon: data.icon
                });
                loaderOut()
                $('#events').DataTable().ajax.reload();
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
                loaderOut()
            }
        });
      }
    })
}
</script>
@endsection