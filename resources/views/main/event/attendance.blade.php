@extends('layouts.register')
@section('content')
<div class="page-header">
    <h3 class="page-title col-md-6 offset-md-3">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> ATTENDANCE
    </h3>
</div>
<div class="row mt-4">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-body data">
                <form  id="memberForm" onsubmit="saveAttendance(this)" enctype="multipart/form-data" data-parsley-validate=""
                    class="form-horizontal form-label-left row" novalidate="">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" placeholder="" class="form-control" value="{{$id}}" style="font-size: 18px;">
                    <input type="hidden" name="type" placeholder="" class="form-control" value="{{$type}}" style="font-size: 18px;">
                        <div class="form-group col-md-12">
                            <label>Enter Registered Email</label>
                            <input type="email" name="email" placeholder="" class="form-control" style="font-size: 18px;">
                        </div>
                        <div class="form-group col-md-6 offset-md-3">
                            <button class="btn btn-inverse-primary btn-lg btn-block">Register</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$('#memberForm').on('submit', function(e) {
    e.preventDefault();
});

function saveAttendance(formData) {
    loaderIn()
    var url = "{{ route('x_saveAttendance')}}";
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(formData),
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            Swal.fire({
                title: data.title,
                html:  data.message,
                icon: data.icon
            });
            loaderOut()
        },
        error: function(data) {
            message = 'We are unable to process request';
            if (data.responseJSON !== undefined) {
                message = '';
                for (var i in data.responseJSON.errors) {
                    var d = data.responseJSON.errors[i];
                    message += d + '<br>';
                }
            }
            Swal.fire({
                title: 'Error',
                html: message,
                icon: 'error'
            });
            loaderOut()
        }
    });
}
</script>
@endsection