@extends('layouts.main')
@section('content')
<?php

use App\Http\Controllers\EventController; ?>
<div class="page-header">
    <div id="outdiv"></div>
    <canvas id="qr-canvas"></canvas>
</div>
<input id="EventID_" value="{{$id}}" type="hidden">
<input id="MemberCode_" value="{{encrypt($code)}}" type="hidden">

<div class="card mt-4 EventSystem">
    <div class="card-body pt-5">
        <label class="control-label h7">Select Schedule</label>
                <div class="mb-3 row pb-3 border-bottom">
                    @foreach($f as $k)
                    @if($c->attendancetype == 'once')
                    <div class="col-md-3  col-xs-12">
                        <div class="form-chec form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="sched" onclick="ifSelect(this)" value="{{$k->id}}" {{ (date('Y-m-d',strtotime($k->date)) != date('Y-m-d')?'disabled':'') }}>
                                {{date('M. d, Y',strtotime($k->date))}}
                            </label>
                            <div class="text-primary font-weight-bold">{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}}</div>
                        </div>
                    </div>
                    @else
                    <div class="col-md-3 col-xs-12">
                        <div class="form-chec form-check-inline pr-3"  data-cooltipz-dir='right' aria-label='{{ (date('Y-m-d',strtotime($k->date)) != date('Y-m-d')?'Disabled':'') }}'>
                            <label class="form-check-label">
                                @if(($k->date == date('Y-m-d')) && (strtotime($k->out) > strtotime('now')))
                                @endif
                                <input type="radio" class="form-check-input" name="sched"   onclick="ifSelect(this)" value="{{$k->id}}" {{ (date('Y-m-d',strtotime($k->date)) != date('Y-m-d')?'disabled':'') }}>
                                {{date('M. d, Y',strtotime($k->date))}}
                            </label>
                            <div class="text-primary font-weight-bold">{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}}</div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if($c->attendancetype == 'once')
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="mode" value="once" checked readonly>
                                In
                            </label>
                        </div>
                    @else
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="mode" value="in" checked>
                                    In
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="mode" value="out">
                                    Out
                                </label>
                            </div>
                    @endif
                    </div>
                </div>
                <div id="mainbody">
                    <div class="row border-top mt-4 pt-3">
                        <div class="col-md-8">
                            <label>Result</label>
                            <div id="result"></div>
                        </div>
                    </div>
                </div>
    </div>
</div>

<div id="link"></div>
<style>
    #outdiv video {
        height: 300px;
        border-radius: 1em;
    }

    canvas {
        visibility: hidden;
    }
    footer{
        display: none;
    }
</style>
@endsection
@section('scripts')
<script src="{{ asset('scanner/qr.js') }}"></script>
<script src="{{ asset('scanner/webqr.js') }}"></script>
<script type="text/javascript">
    $('input[name="sched"]:checked').val()
    $("#outdiv").hide()
    function  ifSelect(a){
        if(a.checked){
            $("#outdiv").show()
        }else{
            $("#outdiv").hide()
        }
    }
    load();
    $( "#outdiv" ).draggable();
    function receiver(a) {
        loaderIn()
        var formData = new FormData(formData);
        formData.append('code', a)
        formData.append('event_id', $('#EventID_').val())
        formData.append('mode', $('input[name="mode"]:checked:enabled').val())
        formData.append('sched', $('input[name="sched"]:checked:enabled').val())
        formData.append('MemberCode_', $('#MemberCode_').val())

        var url = "{{ route('x_savef2fAttendance')}}";
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                document.getElementById("result").innerHTML = "<h2 class='"+data.icon+"'>" + data.message + "</h2>";
                toastr.show("<div class='"+data.icon+"'>" + data.message + "</div>")
                loaderOut()
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
                loaderOut()
            }
        });
    }
</script>

</script>
@endsection