@extends('layouts.main')
@section('content')
<?php
use App\Http\Controllers\EventController;
use \Endroid\QrCode\QrCode as QRID;
use App\Traits\TQRID;
?>
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Events Details
    </h3>
    <a href="{{route('fAttendance',[$id,encrypt($code)])}}" target="_blank" class="btn btn-inverse-primary">Attendance</a>
</div>

<input id="EventID_" value="{{$id}}" type="hidden">
<input id="MemberCode_" value="{{encrypt($code)}}" type="hidden">

<div class="card mt-4">
    <div class="card-body pt-5">
        <div class="row">
            <div class="col-md-3 text-center">
                <h5 id="EventQRImg"></h5>
                @if(isset($permissions['event_edit']))
                <button class="btn btn-inverse-primary btn-rounded" onclick="EditEvent()" id="EditButton">Edit Event</button>
                @endif
            </div>
            <div class="col-md-9 row border-left">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="h7 control-label">Event Name</label>
                            <h4 id="event_name" class="text-primary"></h4>
                        </div>
                        <div class="col-md-6">
                            <label class="h7 control-label">Event Setting</label>
                            <h5 id="event_setting" class="cusbadge text-success d-block"></h5>
                        </div>
                        <div class="col-md-6">
                            <h5 id="event_details"></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-5 border-top pt-4 row">
                    <div class="col-md-6" style="height:200px;overflow: auto;">
                        <label class="h7 control-label">Event Schedule(s)</label>
                        <h5 id="event_date"></h5>
                    </div>
                    <div class="col-md-6">
                        <label class="h7 control-label">Start of Registration</label>
                        <h5 id="event_StartReg"></h5>

                        <label class="h7 control-label">Deadline of Registration</label>
                        <h5 id="event_deadline"></h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
        <!-- </div> -->
        <div class="row mt-4 border-top">
            <div class="col-md-12">
                <div class="car">
                    <div class="card-bod">
                        <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 d-flex flex-row mt-3 mb-3 rounded">
                            <div class="navbar-menu-wrapper d-flex   bg-transparent">
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                        <a class="nav-link active" data-toggle="pill" href="#tabs-Enrolled">
                                            <i class="mdi mdi-account-multiple-outline"></i>
                                            <span class="menu-title ml-2">Registered</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#tabs-Setting">
                                            <i class="mdi mdi-account-multiple-outline"></i>
                                            <span class="menu-title ml-2">Attendance</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="tab-content pl-4 ">
                            <div id="tabs-Enrolled" class="tab-pane mt-2 active">
                                <div class="table-responsive">
                                    <table class="table table-bordered " id="members" width="100%" cellspacing="0">
                                        <thead>
                                            <th width="10%">***</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Department</th>
                                            <th>Course</th>
                                            <th>YearSection</th>
                                            <th>***</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="tabs-Setting" class="tab-pane mt-2">
                                <table class="table table-bordered" id="attendance">
                                    <thead>
                                        <tr>
                                            <td>Participant Name</td>
                                            @if($c->attendancetype == 'in/out')
                                            @foreach($f as $k)
                                            <td>
                                                {{date('M. d, Y',strtotime($k->date))}}
                                                <small class="d-block">{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}}</small>
                                                <div class="d-flex">
                                                    <div class="form-check">
                                                        <label class="form-check-labe">
                                                            <input type="checkbox" class="form-check-input" name="a" onclick="enableAttendance('{{encrypt($k->id)}}','{{$k->in_stat}}','0','in')" {{($k->in_stat==1)?"checked":""}}>
                                                            In <br>
                                                        </label>
                                                    </div>

                                                    <div class="form-check ml-4">
                                                        <label class="form-check-labe">
                                                            <input type="checkbox" class="form-check-input" name="a" onclick="enableAttendance('{{encrypt($k->id)}}','{{$k->out_stat}}','0','out')" {{($k->out_stat==1)?'checked':''}}>
                                                            Out
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- <th>
                                                <div class="form-check">
                                                    <label class="form-check-labe">
                                                        <input type="checkbox" class="form-check-input" name="a" onclick="enableAttendance('{{encrypt($k->id)}}','{{$k->out_stat}}','0','out')" {{($k->out_stat==1)?'checked':''}}>
                                                        Out
                                                    </label>
                                                </div>
                                            </th> -->
                                            @endforeach
                                            @else
                                            @foreach($f as $k)
                                            <td>
                                                {{date('M. d, Y',strtotime($k->date))}}<br>
                                                <div class="form-check">
                                                    <label class="form-check-labe">
                                                        <input type="checkbox" class="form-check-input" name="a" onclick="enableAttendance('{{encrypt($k->id)}}','once','{{$k->enable}}')" {{($k->enable==1?'checked':'')}}>
                                                        {{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}}
                                                    </label>
                                                </div>
                                            </td>
                                            @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($h as $i)
                                        <tr class="">
                                            <td>{{strtoupper($i->Name)}}</td>
                                            @if($c->attendancetype == 'in/out')
                                            @foreach($f as $k)
                                            <?php $in = EventController::getStatusAttendance($k->id, $i->email, 'in'); ?>
                                            <?php $out = EventController::getStatusAttendance($k->id, $i->email, 'out'); ?>
                                            <td><?= $in.' -> '.$out ?></td>
                                            @endforeach
                                            @else
                                            @foreach($f as $k)
                                            <td>{{EventController::getStatusAttendance($k->id,$i->email,'once')}}</td>
                                            @endforeach
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body row text-center">
                @if($c->attendancetype == 'in/out')
                    @foreach($f as $k)
                    <div class="col-md-3  border p-3 py-5 m-3">
                        <button class="btn btn-inverse-primary" onclick="copy('{{$root}}/event/attendance/in/{{encrypt($k->id)}}')">Copy Link</button>
                        <br>
                        {{date('M. d, Y',strtotime($k->date))}}<br><small>{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}} (<b>IN</b>)</small>
                    </div>
                    <div class="col-md-3 border p-3 py-5 m-3">
                        <button class="btn btn-inverse-primary" onclick="copy('{{$root}}/event/attendance/out/{{encrypt($k->id)}}')">Copy Link</button>
                        <br>
                        {{date('M. d, Y',strtotime($k->date))}}<br><small>{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}} (<b>OUT</b>)</small>
                    </div>
                    @endforeach
                    @else
                    @foreach($f as $k)
                    <div class="col-md-3 border p-3 py-5 m-3">
                        <button class="btn btn-inverse-primary" onclick="copy('{{$root}}/event/attendance/in/{{encrypt($k->id)}}')">Copy Link</button>
                        <br>
                        {{date('M. d, Y',strtotime($k->date))}}<br><small>{{date('h:i:s A',strtotime($k->in))}} -> {{date('h:i:s A',strtotime($k->out))}}</small>
                    </div>
                    @endforeach
                    @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editEvent" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Event</h5>
            </div>
            <div class="modal-body">
                <form id="eventEditForm" onsubmit="saveEditEvent(this)" enctype="multipart/form-data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                    {{ csrf_field() }}
                    <input type="hidden" name="event_id" class="form-control">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <h5>Event Name</h5>
                                <div>
                                    <textarea class="form-control" cols="100" rows="3" name="event_name"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Start of Registration</h5>
                                <div class="d-flex">
                                    <input type="date" name="startReg" class="form-control" >
                                    <input type="time" name="startRegtime" class="form-control ml-1" placeholder="0:00" >
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Deadline of Registration</h5>
                                <div class="d-flex">
                                    <input type="date" name="deadline" class="form-control">
                                    <input type="time" name="dtime" class="form-control">
                                </div>
                            </div>
                            <h4 class="pt-3">Event Setting</h4>
                            <div class="row">
                                <div class="col-md-12 row">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="form-check form-check-succes">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="setting" value="f2f" checked onclick="showPlatform(this.value)">
                                                Face to Face
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <div class="form-check form-check-success">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="setting" value="online" onclick="showPlatform(this.value)">
                                                Online Setting
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-5 eventPlatform" style="display: none;">
                                <h4 class="pt-5">Event Platform</h4>
                                <small>Platform used in Meeting</small>
                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <div class="form-check form-check-succes">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="platform" value="zoom" onclick="showMeeting(this.value)">
                                                Zoom Meeting
                                            </label>
                                        </div>
                                        <div class="form-check form-check-success">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="platform" value="gmeet" onclick="showMeeting(this.value)">
                                                Google Meet
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group gmeet">
                                            <label>Google Link</label>
                                            <input type="" name="gmeetlink" class="form-control">
                                        </div>
                                        <div class="form-group zoom">
                                            <label>ZoomID</label>
                                            <input type="" name="zoomID" class="form-control">
                                            <label class="mt-2">Zoom Passcode</label>
                                            <input type="" name="zoomPass" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 border-left">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Strict Late?</label>
                                        <div class="">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="strict" value="1">
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="strict" value="0" checked>
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Type of Attendance</label>
                                        <div class="">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="type" value="in/out" checked>
                                                    In & Out
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="type" value="once">
                                                    Once per session
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-inverse-primary mt-4" type="button" onclick="addRowSchedule()">Add New Row</button>
                            <small class="d-block mt-2">Add new row for next set of attendance</small>
                            <div class="row">
                                <div class="row mt-2 ml-0 ml-0" id="schedule">
                                    <div class="col-md-4">
                                        <h5>Date of Event</h5>
                                        <div>
                                            <input name="doe[]" class="form-control" type="date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Time In</h5>
                                        <div>
                                            <input type="time" name="timein[]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Time Out</h5>
                                        <div>
                                            <input type="time" name="timeout[]" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="schedule_add" class="col-md-12"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row border-top pt-4 mt-3">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="link"></div>
@endsection
@section('scripts')
<script type="text/javascript">
    function copy(h){
        $("#link").show()
        toastr.show('<h1 class="mdi mdi-check-circle-outline text-success" style="font-size:65px"></h1>Link for Attendance was copied to Clipboard')
        $("#link").html(h)
        selection = window.getSelection();
        selection.removeAllRanges();
        var range = document.createRange();
        range.selectNode(document.getElementById('link'));
        window.getSelection().addRange(range);
        document.execCommand("copy");
        $("#link").hide()
    }
    var i = 0;

    function removeSchedule3(id) {
        $('#schedule' + id).remove()
    }

    function addRowSchedule() {
        i++
        // if (i <= 4) {
        var uid = randNum()
        var h = ''
        h += '<button type="button" class="btn btn-danger btn-rounded btn-icon pull-right mt-4" onclick="removeSchedule3(\'' + i +
            '\')"><i class="mdi mdi-window-close"></i> </button>'

        $('#schedule').clone().attr('id', 'schedule' + i).appendTo('#schedule_add');
        $("#schedule_add #schedule" + i).append(h)
        // }
    }

    function showMeeting(a) {
        if (a == 'zoom') {
            $(".zoom input").attr('disabled', false)
            $(".gmeet input").attr('disabled', true)
            $(".zoom").show()
            $(".gmeet").hide()
        }
        if (a == 'gmeet') {
            $(".gmeet input").attr('disabled', false)
            $(".zoom input").attr('disabled', true)
            $(".gmeet").show()
            $(".zoom").hide()
        }
    }

    function showPlatform(a) {
        if (a == 'online') {
            $(".eventPlatform input").attr('disabled', false)
            $(".eventPlatform").show()
        } else {
            $(".eventPlatform input").attr('disabled', true)
            $(".eventPlatform").hide()
        }
    }


    $('#eventEditForm').on('submit', function(e) {
        e.preventDefault();
    });

    function saveEditEvent(formData) {
        loaderIn()
        var url = "{{ route('x_saveEditEvent')}}";
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(formData),
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                Swal.fire({
                    title: data.title,
                    html: data.message,
                    icon: data.icon
                });
                loaderOut()
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
                loaderOut()
            }
        });
    }

    getData()
    setInterval(function() {
        getData()
    }, 10000)

    function getData() {
        var formData = new FormData();
        formData.append('id', $("#EventID_").val());
        $.ajax({
            type: "POST",
            url: "{{route('x_eventInfo')}}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                $("#event_date").html(data[3]);
                $("#event_setting").html(data[0].event_setting + (data[0].platform == 'online' ? ' [' + data[0].platform + ']' : ''))
                $("#event_name").html(data[0].event_name)
                $("#EventQRImg").html(data[1])
                $("#EventQR").html(data[0].event_code)
                $("#event_details").html(data[2])
                $("#event_deadline").html(data[4])
                $("#event_StartReg").html(data[7])
                // $("#attendance").html(data[5])
                if (data[6] == 'done') {
                    $("#EditButton").hide()
                }

                // $("#MemberCode_").val(data[0].event_code)
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
            }
        });
    }

    function enableAttendance(a, b, c, d = false) {
        var url = "{{ route('x_enableAttendance')}}";
        var formData = new FormData();
        formData.append('a', a)
        formData.append('b', b)
        formData.append('c', c)
        formData.append('d', d)
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                getData()
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
                loaderOut()
            }
        });

        // if(b == 0){
        // $("#link").show()
        // var h = location.hostname + '/event/attendance/' + (d == false ? 'once' : d) + '/' + a

        // toastr.show('<h1 class="mdi mdi-check-circle-outline text-success" style="font-size:65px"></h1>Link for Attendance was copied to Clipboard')
        // $("#link").html(h)

        // selection = window.getSelection();
        // selection.removeAllRanges();
        // var range = document.createRange();
        // range.selectNode(document.getElementById('link'));
        // window.getSelection().addRange(range);
        // document.execCommand("copy");
        // $("#link").hide()

        // location.reload()
        // }
    }

    function setAsAccomplished(a) {
        var formData = new FormData();
        formData.append('event_code', a)
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.isConfirmed) {
                loaderIn()
                var url = "{{ route('x_SetAsDone')}}";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(data) {
                        Swal.fire({
                            title: data.title,
                            html: data.message,
                            icon: data.icon
                        });
                        loaderOut()
                    },
                    error: function(data) {
                        message = 'We are unable to process request.';
                        if (data.responseJSON !== undefined) {
                            message = '';
                            for (var i in data.responseJSON.errors) {
                                var d = data.responseJSON.errors[i];
                                message += d + '<br>';
                            }
                        }
                        Swal.fire({
                            title: 'Error',
                            html: message,
                            icon: 'error'
                        });
                        loaderOut()
                    }
                });
            }
        })
    }

    function copyToClipboard(code) {
        $("#link").show()
        var h = location.hostname + '/event/' + code

        toastr.show('<h1 class="mdi mdi-check-circle-outline text-success" style="font-size:65px"></h1> Copied to Clipboard')
        $("#link").html(h)

        selection = window.getSelection();
        selection.removeAllRanges();
        var range = document.createRange();
        range.selectNode(document.getElementById('link'));
        window.getSelection().addRange(range);
        document.execCommand("copy");
        $("#link").hide()
    }

    $("#attendance").DataTable({
        responsive: true,
        dom: 'Bfrtip',
        header: true,
        footer: true,
        "bAutoWidth": true,
        lengthMenu: [
            [10, 50, 100, 500, 1000, 2000, 3000, 5000, 100000],
            ['10 rows', '50 rows', '100 rows', '500 rows', '1000 rows', '2000 rows', '3000 rows', '5000 rows',
                'Show all'
            ]
        ],
        buttons: [
            'pageLength',
            {
                extend: 'copy',
                text: 'Copy to clipboard'
            },
            {
                extend: 'excel',
                download: 'open',
                title: $("#event_name").text(),
            },
            {
            extend: 'pdfHtml5',
            orientation: 'portrait',
            pageSize: 'LEGAL',
            title: $("#event_name").text(),

            customize: function(doc) {
                doc.styles.title = {
                    color: '#012259',
                    fontSize: '14',
                    alignment: 'center'
                };

                doc.styles.message = {
                    color: '#595401',
                    fontSize: '9',
                    alignment: 'left'
                };

            }
        }
        ]
    })

    $('#members').DataTable({
        responsive: true,
        "processing": true,
        "serverSide": true,
        columnDefs: [{
            orderable: false,
            targets: [0, 1, 2, 3, 4, 5]
        }],
        "ajax": {
            url: "{{route('x_members')}}",
            "data": function(d) {
                d.MemberCode = $("#MemberCode_").val()
            },
        },
        bJQueryUI: false,
        bAutoWidth: false,
        orderCellsTop: true,
        fixedHeader: true,
        "sPaginationType": "full_numbers",
        "sdom": '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        "minimumResultsForSearch": "-5",
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 50, 100, 500, 1000, 2000, 3000, 5000, 100000],
            ['10 rows', '50 rows', '100 rows', '500 rows', '1000 rows', '2000 rows', '3000 rows', '5000 rows', 'Show all']
        ],
        buttons: [
            'pageLength',
            {
                extend: 'copy',
                text: 'Copy to clipboard'
            },
            {
                extend: 'excel',
                download: 'open'
            },
            {
                extend: 'pdfHtml5',
                download: 'open'
            }
        ]

    });


    function EditEvent() {
        var editEvent = new bootstrap.Modal($('#editEvent'), {
            keyboard: false
        })
        editEvent.show();

        var formData = new FormData();
        formData.append('id', $("#EventID_").val());
        $.ajax({
            type: "POST",
            url: "{{route('x_editEvent')}}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                var date = data.deadline.split(" ");
                var sdate = data.startDate.split(" ");
                console.log(date[0])
                $("[name = 'event_name']").val(data[0].event_name)
                $("[name = 'event_id']").val(data[0].ids)
                $("[name = 'startReg']").val(sdate[0])
                $("[name = 'startRegtime']").val(sdate[1])
                $("[name = 'deadline']").val(date[0])
                $("[name = 'dtime']").val(date[1])
                $("[name = 'setting'][value='" + data[0].event_setting + "']").attr('checked', true)
                $("[name = 'strict'][value='" + data[0].strict_late + "']").attr('checked', true)
                $("[name = 'type'][value='" + data[0].attendancetype + "']").attr('checked', true)
                $("[name = 'platform'][value='" + data[0].platform + "']").attr('checked', true)
                $("[name = 'zoomID']").val(data[0].username)
                $("[name = 'zoomPass']").val(data[0].passcode)

                startReg
                // event_StartReg
                var html = ''
                console.log(data[1])
                if (data[1] == 0) {
                    html += '<div class="row mt-2 ml-0 ml-0">'
                    for (var k in data.scheds) {
                        var d = data.scheds[k]
                        html += '<div class="col-md-4">'
                        html += '<h5>Date of Event</h5>'
                        html += '<div>'
                        html += '<input name="doe[]" class="form-control" type="date" value="' + d.date + '">'
                        html += '</div>'
                        html += '</div>'
                        html += '<div class="col-md-3">'
                        html += '<h5>Time In</h5>'
                        html += '<div>'
                        html += '<input type="time" name="timein[]" class="form-control"  value="' + d.in + '">'
                        html += '</div>'
                        html += '</div>'
                        html += '<div class="col-md-3">'
                        html += '<h5>Time Out</h5>'
                        html += '<div>'
                        html += '<input type="time" name="timeout[]" class="form-control"  value="' + d.out + '">'
                        html += '</div>'
                        html += '</div>'
                    }
                    html += '</div>'
                } else {
                    html = "You cannot update the schedule"
                }

                $("#schedule_add").html(html)
            },
            error: function(data) {
                message = 'We are unable to process request.';
                if (data.responseJSON !== undefined) {
                    message = '';
                    for (var i in data.responseJSON.errors) {
                        var d = data.responseJSON.errors[i];
                        message += d + '<br>';
                    }
                }
                Swal.fire({
                    title: 'Error',
                    html: message,
                    icon: 'error'
                });
            }
        });

    }
</script>
@endsection