@extends('layouts.main')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Add Event
    </h3>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form  id="eventForm" onsubmit="saveEvent(this)" enctype="multipart/form-data" data-parsley-validate=""
                class="form-horizontal form-label-left" novalidate="">
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <h5>Event Name</h5>
                                <div>
                                    <textarea class="form-control" cols="100" rows="3" name="event_name"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Start of Registration</h5>
                                <div class="d-flex">
                                    <input type="date" name="startReg" class="form-control" >
                                    <input type="time" name="startRegtime" class="form-control ml-1" placeholder="0:00" >
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Deadline of Registration</h5>
                                <div class="d-flex">
                                    <input type="date" name="deadline" class="form-control" >
                                    <input type="time" name="dtime" class="form-control ml-1" placeholder="0:00" >
                                </div>
                            </div>
                            <h4 class="pt-3">Event Setting</h4>
                            <div class="row">
                                <!-- <div class="col-md-12 row"> -->
                                    <div class="col-md-6 col-xs-6">
                                        <div class="form-check form-check-succes">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="setting" value="f2f" checked onclick="showPlatform(this.value)">
                                                Face to Face
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                            <div class="form-check form-check-success">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="setting" value="online"  onclick="showPlatform(this.value)">
                                                    Online Setting
                                                </label>
                                            </div>
                                    </div>
                                <!-- </div> -->
                            </div>
                            <div class="mb-5 eventPlatform" style="display: none;">
                            <h4 class="pt-5">Event Platform</h4>
                            <small>Platform used in Meeting</small>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <div class="form-check form-check-succes">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="platform" value="zoom" onclick="showMeeting(this.value)">
                                            Zoom Meeting
                                        </label>
                                    </div>
                                    <div class="form-check form-check-success">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="platform" value="gmeet" onclick="showMeeting(this.value)">
                                                Google Meet
                                            </label>
                                        </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group gmeet">
                                        <label>Google Link</label>
                                        <input type="" name="gmeetlink" class="form-control">
                                    </div>
                                    <div class="form-group zoom">
                                        <label>ZoomID</label>
                                        <input type="" name="zoomID" class="form-control">
                                        <label class="mt-2">Zoom Passcode</label>
                                        <input type="" name="zoomPass" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>  
                        </div>
                        <div class="col-md-7 border-left">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Strict Late?</label>
                                    <div class="">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="strict" value="1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="strict" value="0" checked>
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type of Attendance</label>
                                    <div class="">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="type" value="in/out" checked>
                                                In & Out
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="type" value="once">
                                                Once per session
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <small class="d-block mt-2">Add new row for next set of attendance</small>
                            <div class="row">
                                <div class="row mt-2 ml-0 ml-0" id="schedule">
                                    <div class="col-md-4">
                                        <h5>Date of Event</h5>
                                        <div>
                                            <input name="doe[]" class="form-control" type="date" value="{{date('Y-m-d')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <h5>Time In</h5>
                                        <div>
                                            <input type="time" name="timein[]" class="form-control"  value="{{date('H:i:s',)}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <h5>Time Out</h5>
                                        <div>
                                            <input type="time" name="timeout[]" class="form-control" value="{{date('H:i:s',strtotime('+2 hours'))}}">
                                        </div>
                                    </div>
                                    <button class="btn btn-inverse-primary btn-sm p-2 mt-4" type="button"  id="addButton" onclick="addRowSchedule()"><i class="mdi mdi-plus-circle"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div id="schedule_add" class="col-md-12"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row border-top pt-4 mt-3">
                        <div class="col-md-12">
                            <button class="btn btn-inverse-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
var i = 0;
function removeSchedule3(id) {
    $('#schedule' + id).remove()
}

function addRowSchedule() {
    i++
    // if (i <= 4) {
        var uid = randNum()
        var h = ''
            h +='<button type="button" class="btn btn-danger btn-rounded btn-icon pull-right mt-4" onclick="removeSchedule3(\'' + i +
            '\')"><i class="mdi mdi-window-close"></i> </button>'

        $('#schedule').clone().attr('id', 'schedule' + i).appendTo('#schedule_add');
        $('#schedule' + i +' input').val(' ');
        $('#schedule' + i +' #addButton').remove();
        
        $("#schedule_add #schedule" + i).append(h)
    // }
}


 $(".TP").daterangepicker({
    forceUpdate: true
});

$('#eventForm').on('submit', function(e) {
    e.preventDefault();
});


function showMeeting(a){
    if(a == 'zoom'){
        $(".zoom input").attr('disabled',false)
        $(".gmeet input").attr('disabled',true)
        $(".zoom").show()
        $(".gmeet").hide()
    }
    if(a == 'gmeet'){
        $(".gmeet input").attr('disabled',false)
        $(".zoom input").attr('disabled',true)
        $(".gmeet").show()
        $(".zoom").hide()
    }
}

function showPlatform(a){
    if(a == 'online'){
        $(".eventPlatform input").attr('disabled',false)
        $(".eventPlatform").show()
    }else{
        $(".eventPlatform input").attr('disabled',true)
        $(".eventPlatform").hide()
    }
}
function saveEvent(formData) {
    loaderIn()
    var url = "{{ route('x_saveEvent')}}";
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(formData),
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            Swal.fire({
                title: data.title,
                html:  data.message,
                icon: data.icon
            });
            loaderOut()
        },
        error: function(data) {
            message = 'We are unable to process request.';
            if (data.responseJSON !== undefined) {
                message = '';
                for (var i in data.responseJSON.errors) {
                    var d = data.responseJSON.errors[i];
                    message += d + '<br>';
                }
            }
            Swal.fire({
                title: 'Error',
                html: message,
                icon: 'error'
            });
            loaderOut()
        }
    });
}

</script>
@endsection