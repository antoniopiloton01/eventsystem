@extends('layouts.main')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-primary text-white mr-2">
            <i class="mdi mdi-view-dashboard"></i>
        </span> Users
    </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered " id="myUsers" width="100%" cellspacing="0">
                    <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Account Status</th>
                        <th>***</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$('#myUsers').dataTable({
  processing: true,
  serverSide: true,
  paging: true,
  ajax: "{{route('x_users')}}",
  columnDefs: [{
    "targets": [0, 1],
    "orderable": true,
    "searchable": true,
  }, {
    "targets": [1,2,3],
    "orderable": false,
    "searchable": false,
  },
   {
      "targets": [1,2,3],
  },
  ],
  bJQueryUI: false,
  bAutoWidth: false,
  sPaginationType: "full_numbers",
  responsive: true,
  // sDom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
  // dom: 'Bfrtip',
  // minimumResultsForSearch: "-5",
  // lengthMenu: [
  //   [10, 25, 50, 100],
  //   [10, 25, 50, 100]
  // ],
  // lengthChange: false,
  // buttons: [ 'copy', 'excel', 'pdf',  ]

  dom: 'Bfrtip',
  lengthMenu: [
      [ 10, 50, 100, 500, 1000, 2000, 3000, 5000, 100000],
      [ '10 rows', '50 rows', '100 rows', '500 rows', '1000 rows', '2000 rows', '3000 rows', '5000 rows', 'Show all' ]
  ],
  buttons: [
      'pageLength',
      {
      extend: 'copy',
      text: 'Copy to clipboard'
  },
  {
    extend: 'excel',
    download: 'open'
            },
  {
    extend: 'pdfHtml5',
    download: 'open'
            }
  ]
});
$(".dataTables_length select").select2({
  minimumResultsForSearch: "-5"
});

</script>
@endsection