@extends('layouts.main')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-success text-white mr-2">
            <i class="mdi mdi-calendar-text"></i>
        </span> {{$user->name.' <'.$user->email.'>'}} Permissions
    </h3>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <form id="permissions"  onsubmit="saveUserPermissions(this)" class="form-horizontal form-label-left" method="POST" action="">
            <div class=" col-lg-6">
              <h4>List of Permissions</h4>
              <select id="userPermissions" name="userPermissions[]" class="select" multiple="true" style="height:300px;width: 350px;">
                @foreach($cpermissions as $p)
                  <option value="{{$p->id}}" {{($p->selected==true?'selected':'')}}>{{$p->details}}</option>
                @endforeach
              </select>
              <button type="submit" class="btn btn-sm btn-inverse-primary mt-2">Save</button>
            </div>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
var demo2 = $('select[name="userPermissions[]"').bootstrapDualListbox({
  nonSelectedListLabel: 'Non-selected',
  selectedListLabel: 'Current Permissions',
  preserveSelectionOnMove: 'moved',
  moveOnSelect: false,
  sortByInputOrder: 'true',
  nonSelectedFilter: ''
});

$("#demoform").submit(function() {
  alert($('[name="userPermissions[]"]').val());
  return false;
});

$('#permissions').on('submit', function(e) {
  e.preventDefault();
});

function saveUserPermissions(form) {
  var formData=new FormData(form);
  var url = "{{ route('x_saveUserPermissions',encrypt($user->id))}}";
  var selected=$('[name="userPermissions[]"]').val();
  formData.append('selectedPermissions',selected);
  $.ajax({
    type: "POST",
    url: url,
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    cache: false,
    async: false,
    success: function(data) {
      toastr.show(data.message)
    },
    error: function(data) {
      message = 'We are unable to process request.';
      if (data.responseJSON !== undefined) {
        message = '';
        for (var i in data.responseJSON.errors) {
          var d = data.responseJSON.errors[i];
          message += d + '<br>';
        }
      }
      toastr.show(message)
    }
  });
}
</script>
@endsection
