@extends('layouts.app')
@section('content')
<section>
  <div class="container mt-5">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 col-lg-8 col-xl-6">
        <div class="row">
          <div class="col text-center">
            <h1>Register</h1>
            <!-- <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia. </p> -->
          </div>
        </div>
        <form class="form-horizontal form-label-left" method="POST" id="SAVE" onsubmit="saveUser(this)">
          {{ csrf_field() }}
          <div class="row align-items-center">
            <div class="col mt-4">
              <label class="control-label">Fullname</label>
              <input type="text" class="form-control" placeholder="Name" name="name">
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <label class="control-label">Email</label>
              <input type="email" class="form-control" placeholder="Email" name="email" onblur="validate(this.value)">
              <span class="h7 email-validate"></span>
            </div>
          </div>

          <div class="align-items-center mt-4">
          <label class="control-label">Password</label>
            <div class="d-flex">
              <div class="input-group mr-5">
              <input type="password" class="form-control" name="password" id="pass">
              <span class="input-group-text"><i class="mdi mdi-eye toggle-password"  toggle="#pass" ></i></span>
            </div>
            <div class="input-group">
              <input type="password" class="form-control"  id="pass_con" name="password_confirmation">
              <span class="input-group-text"><i class="mdi mdi-eye toggle-password"  toggle="#pass_con" ></i></span>
            </div>
            </div>
          </div>
          <div class="row justify-content-start mt-4">
            <div class="col">
              <button class="btn btn-primary mt-4">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
  $(".toggle-password").click(function() {

$(this).toggleClass("mdi-eye mdi-eye-off");
var input = $($(this).attr("toggle"));
if (input.attr("type") == "password") {
  input.attr("type", "text");
} else {
  input.attr("type", "password");
}
});

  $('#SAVE').on('submit', function(e) {
    e.preventDefault();
  });

  function validate(email) {
    var a = email.split('@');
    // console.log(a[1])
    if (a[1] != 'usa.edu.ph') {
      $(".email-validate").html('Email is not valid in this University').addClass('text-danger')
    } else {
      $(".email-validate").html('Okay').addClass('text-success')
    }
  }

  function saveUser(formData) {
    loaderIn()
    var url = "{{ route('add_user') }}";
    $.ajax({
      type: "POST",
      url: url,
      data: new FormData(formData),
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      async: false,
      success: function(data) {
        if (data.result == true) {
          console.log(data.email)
          window.location.href = data.email
        } else {
          Swal.fire({
            title: data.title,
            html: data.message,
            icon: data.icon
          });
        }
        loaderOut()
      },
      error: function(data) {
        message = 'We are unable to process request.';
        if (data.responseJSON !== undefined) {
          message = '';
          for (var i in data.responseJSON.errors) {
            var d = data.responseJSON.errors[i];
            message += d + '<br>';
          }
        }
        Swal.fire({
          title: 'Error',
          html: message,
          icon: 'error'
        });
        loaderOut()
      }
    });
  }
</script>
@endsection