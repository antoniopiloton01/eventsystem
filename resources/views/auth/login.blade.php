@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>USA Attendance System</title>
    <link href="{{ asset('/images/L.png') }}" rel="shortcut icon">
    <link href="{{ asset('theme/assets/vendors/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <meta name="theme-color" content="#50EDE2">
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet">
    <meta name="theme-color" content="#3093F0">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#3093F0">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#3093F0">
</head>
<body data-theme="blue">
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth">
                <div class="row flex-grow">
                    <div class="col-md-5 mx-auto">
                        <div class="auth-form-light text-left p-5 shadow">
                            <div class="text-center mb-5 dsdsd">
                                <img src="{{ asset('images/noname.png') }}" width="20%">
                                <div class="d">
                                    <span>USA</span><span>Event</span>
                                    <div class="h1 d-none" style="font-family: 'Mistral';transform: translateY(-.5em) translateX(2.5em);-webkit-text-stroke-width: 1px;-webkit-text-stroke-color: black;-webkit-text-fill-color: white">Management System</div>
                                </div>
                            </div>
                            <!-- <h4>Hello! let's get started</h4> -->
                            <h6 class="font-weight-light">Sign in to continue</h6>
                            <form id="loginForm" role="form" class="form__sign-in pt-3" method="POST"
                                action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-lg" id=""
                                        placeholder="Username" name="email">
                                    @if ($errors->has('email'))
                                    <label class="font-weight-bold mt-2">
                                        {{ $errors->first('email') }}
                                    </label>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-control-lg" id=""
                                        placeholder="Password" name="password">
                                    @if ($errors->has('password'))
                                    <label class="font-weight-bold mt-2">
                                        {{ $errors->first('password') }}
                                    </label>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <button
                                        class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN
                                        IN</button>
                                </div>
                            </form>
                            <hr>
                            <div class="text-center row">
                                <div class="col-md-6">
                                    <a href="{{route('register_user')}}">Register</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{route('reset_password')}}">Forgot Password</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
</body>
@endsection
