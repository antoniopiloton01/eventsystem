<title>You have been Successfully verified your email account</title>
<link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('theme/assets/vendors/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('/images/noname.png') }}" rel="shortcut icon">
<div class="body">
    <img src="{{asset('images/noname.png')}}" width="100px">
    <div class="d">
        <span>University of</span><span>San Agustin</span>
    </div>
    <div class="mt-5 mb-5 text-success">
        <i class="mdi mdi-check-circle-outline d-block"></i>
        SUCCESS
    </div>
    <h4>You have been Successfully verified your email account</h4>
    <br><br>
    @if(isset($QR))
    <div class="mb-5">
        <img src="{{$QR}}">
        <small class="d-block">Present this QR Code to the staff during Face to Face Setting</small>
    </div>
    @endif
    <a href="/">Go to Homepage</a>
</div>

<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap");

* {
    font-family: 'Poppins';
}

body {
    text-align: center;
    padding: 3em;
}
.mt-5{
    font-size: 25px;
}
.mt-5 i{
    font-size: 50px;

}
h1 {
    margin-top: 3em;
}

a {
    padding: .7em;
    border: 0;
    border-radius: .3em;
    background: rgba(54, 153, 255);
    color: white;
    font-weight: 600px;
    margin-top: 4em;
    text-decoration: none;
}

.d {
    font-size: 18px;
    font-weight: bold;
    margin-top: .5em;
}

.d span:first-child {
    color: rgba(239, 48, 94);
    margin-right: .2em;
    text-shadow: 1px 1px 1px rgb(228, 228, 228);
}

.d span:last-child {
    color: rgba(13, 36, 81)
}
</style>