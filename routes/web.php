<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
})->name('home');

Auth::routes();
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::get('/users', [App\Http\Controllers\UsersController::class, 'users'])->name('users');
Route::get('users/requirements/list/', [App\Http\Controllers\UsersController::class, 'x_users'])->name('x_users');

Route::get('/manage/user/{userID}/permissions/', [App\Http\Controllers\UserController::class, 'userpermissions'])->name('userPermissions');
Route::post('/manage/user/{userID}/permissions/save', [App\Http\Controllers\UserController::class, 'x_saveUserPermissions'])->name('x_saveUserPermissions');
Route::get('/users/a', [App\Http\Controllers\UserController::class, 'addUsers'])->name('addUsers');
Route::post('/users/add', [App\Http\Controllers\UserController::class, 'addUser'])->name('addUser');

// EVENTS
Route::get('event', [App\Http\Controllers\EventController::class, 'events'])->name('events');
Route::get('event/add', [App\Http\Controllers\EventController::class, 'event_add'])->name('event_add');
Route::post('event/add/save', [App\Http\Controllers\EventController::class, 'x_saveEvent'])->name('x_saveEvent');
Route::post('event/del', [App\Http\Controllers\EventController::class, 'x_delEvent'])->name('x_delEvent');
Route::post('event/done', [App\Http\Controllers\EventController::class, 'x_SetAsDone'])->name('x_SetAsDone');
Route::post('event/enable', [App\Http\Controllers\EventController::class, 'x_enableAttendance'])->name('x_enableAttendance');
Route::post('event/edit', [App\Http\Controllers\EventController::class, 'x_editEvent'])->name('x_editEvent');
Route::post('event/edit/save', [App\Http\Controllers\EventController::class, 'x_saveEditEvent'])->name('x_saveEditEvent');


Route::get('events/list/', [App\Http\Controllers\EventController::class, 'x_events'])->name('x_events');
Route::get('events/details/{id}/{code}', [App\Http\Controllers\EventController::class, 'viewDetails'])->name('viewDetails');
Route::post('events/details/more', [App\Http\Controllers\EventController::class, 'x_eventInfo'])->name('x_eventInfo');

Route::get('/event/{code}', [App\Http\Controllers\PublicEventController::class, 'x_register'])->name('x_register');
Route::post('member/add/save', [App\Http\Controllers\PublicEventController::class, 'x_saveMember'])->name('x_saveMember');
Route::get('events/member/list/', [App\Http\Controllers\EventController::class, 'x_members'])->name('x_members');
Route::get('confirm/email/{code}', [App\Http\Controllers\PublicEventController::class, 'x_confirm'])->name('x_confirm');
Route::get('/upcoming', [App\Http\Controllers\EventController::class, 'x_upcoming'])->name('x_upcoming');

// ATTENDANCE
Route::get('/event/attendance/{type}/{id}', [App\Http\Controllers\PublicEventController::class, 'x_attendance'])->name('x_attendance');
Route::post('/event/attendance/save', [App\Http\Controllers\PublicEventController::class, 'x_saveAttendance'])->name('x_saveAttendance');
Route::post('/event/attendance/save/d', [App\Http\Controllers\PublicEventController::class, 'x_savef2fAttendance'])->name('x_savef2fAttendance');
Route::get('/event/attendance/auto', [App\Http\Controllers\EventController::class, 'x_AutoCheckAttendance'])->name('x_AutoCheckAttendance');
Route::get('/user/register/', [App\Http\Controllers\PublicEventController::class, 'register_user'])->name('register_user');
Route::get('/attendance/{id}/{code}', [App\Http\Controllers\EventController::class, 'fAttendance'])->name('fAttendance');

Route::post('/users/save', [App\Http\Controllers\PublicEventController::class, 'add_user'])->name('add_user');
Route::get('/verify/{email}/{code}', [App\Http\Controllers\PublicEventController::class, 'verify'])->name('verify');
Route::get('/resent', [App\Http\Controllers\PublicEventController::class, 'resend'])->name('resend');
Route::get('/toverify/{email}', [App\Http\Controllers\PublicEventController::class, 'toverify'])->name('toverify');

Route::get('/reset', [App\Http\Controllers\PublicEventController::class, 'reset_password'])->name('reset_password');

