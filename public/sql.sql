-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 22, 2022 at 12:57 AM
-- Server version: 8.0.21
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event`
--
CREATE DATABASE IF NOT EXISTS `event` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `event`;

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE IF NOT EXISTS `attendance` (
  `attendanceID` int NOT NULL AUTO_INCREMENT,
  `event_id` int DEFAULT NULL,
  `event_code` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `in` time DEFAULT NULL,
  `out` time DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` mediumint UNSIGNED DEFAULT NULL,
  `updated_by` mediumint UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive','locked','unlocked','filled','pre-enroll','done') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'active',
  `archived` tinyint UNSIGNED DEFAULT '0',
  PRIMARY KEY (`attendanceID`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`attendanceID`, `event_id`, `event_code`, `email`, `in`, `out`, `created_at`, `created_by`, `updated_by`, `status`, `archived`) VALUES
(22, 34, 'BZ21637917423YWM', 'antoniollealdeluibpiloton@gmail.com', '07:06:04', '09:50:13', '2021-11-26 17:06:04', 7, 7, 'active', 0),
(23, 33, 'BZ21637917423YWM', 'antoniollealdeluibpiloton@gmail.com', '13:10:25', '17:18:26', '2021-11-27 10:18:33', NULL, NULL, 'active', 0),
(24, 34, 'BZ21637917423YWM', 'apiloton@ssct.edu.ph', '13:50:25', '15:12:15', '2021-11-28 13:50:25', 7, 7, 'active', 0),
(25, 33, 'BZ21637917423YWM', 'apiloton@ssct.edu.ph', '13:51:41', '13:51:34', '2021-11-28 13:51:34', 7, 7, 'active', 0),
(26, 36, 'APU16379851149PP', 'apiloton@ssct.edu.ph', '11:47:56', '11:58:47', '2021-11-29 11:47:56', 7, 7, 'active', 0),
(27, 42, 'AYQ1638166675N0Z', 'mvallite@usa.edu.ph', '14:25:35', '14:28:38', '2021-11-29 14:25:35', 7, 7, 'active', 0),
(28, 41, 'AYQ1638166675N0Z', 'mvallite@usa.edu.ph', '15:47:02', '12:39:32', '2021-11-29 15:47:02', 7, 7, 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `event_code` varchar(20) DEFAULT NULL,
  `event_name` varchar(100) DEFAULT NULL,
  `event_startReg` datetime DEFAULT NULL,
  `event_deadline` datetime DEFAULT NULL,
  `event_setting` varchar(10) DEFAULT NULL,
  `platform` varchar(10) DEFAULT NULL,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `passcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `glink` varchar(60) DEFAULT NULL,
  `strict_late` tinyint DEFAULT NULL,
  `autoOn` tinyint DEFAULT NULL,
  `attendancetype` set('in/out','once') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` mediumint UNSIGNED DEFAULT NULL,
  `updated_by` mediumint UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive','locked','unlocked','filled','pre-enroll','done') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'active',
  `archived` tinyint UNSIGNED DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_code`, `event_name`, `event_startReg`, `event_deadline`, `event_setting`, `platform`, `username`, `passcode`, `glink`, `strict_late`, `autoOn`, `attendancetype`, `created_at`, `created_by`, `updated_by`, `status`, `archived`) VALUES
(3, 'BZ21637917423YWM', 'SAMPLE 2', NULL, '2021-11-30 08:00:00', 'f2f', NULL, NULL, NULL, NULL, 0, 1, 'in/out', '2021-11-26 17:03:43', 7, 7, 'done', 0),
(5, 'APU16379851149PP', 'SAMPLE 2', NULL, '2021-11-30 13:00:00', 'f2f', NULL, NULL, NULL, NULL, 1, 1, 'in/out', '2021-11-27 11:51:54', 7, 7, 'done', 0),
(6, 'AYQ1638166675N0Z', 'USA INTRAMS', NULL, '2021-11-30 13:00:00', 'online', 'zoom', NULL, NULL, NULL, 0, 1, 'in/out', '2021-11-29 14:17:55', 7, 7, 'done', 0),
(7, 'SDM1642311260SGZ', 'Sample with Start', '2022-01-16 14:24:00', '2022-01-16 15:00:00', 'f2f', NULL, NULL, NULL, NULL, 0, 1, 'in/out', '2022-01-16 13:34:20', 7, 7, 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_details`
--

DROP TABLE IF EXISTS `event_details`;
CREATE TABLE IF NOT EXISTS `event_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `event_code` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `in` time DEFAULT NULL,
  `out` time DEFAULT NULL,
  `in_stat` tinyint DEFAULT '0',
  `out_stat` tinyint DEFAULT '0',
  `enable` tinyint DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` mediumint UNSIGNED DEFAULT NULL,
  `updated_by` mediumint UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive','locked','unlocked','filled','pre-enroll','done') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'active',
  `archived` tinyint UNSIGNED DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_details`
--

INSERT INTO `event_details` (`id`, `event_code`, `date`, `in`, `out`, `in_stat`, `out_stat`, `enable`, `created_at`, `created_by`, `updated_by`, `status`, `archived`) VALUES
(34, 'BZ21637917423YWM', '2021-11-29', '07:00:00', '12:00:00', 0, 0, 0, NULL, NULL, NULL, 'active', 0),
(33, 'BZ21637917423YWM', '2021-11-29', '13:00:00', '17:00:00', 0, 0, 0, NULL, NULL, NULL, 'active', 0),
(44, 'AYQ1638166675N0Z', '2021-12-02', '13:00:00', '17:00:00', 0, 0, 0, NULL, NULL, NULL, 'active', 0),
(36, 'APU16379851149PP', '2021-11-27', '11:51:35', '13:51:35', 0, 0, 1, NULL, NULL, NULL, 'active', 0),
(43, 'AYQ1638166675N0Z', '2021-12-02', '07:00:00', '12:00:00', 0, 0, 0, NULL, NULL, NULL, 'active', 0),
(42, 'AYQ1638166675N0Z', '2021-11-29', '13:00:00', '17:00:00', 0, 0, 0, NULL, NULL, NULL, 'active', 0),
(41, 'AYQ1638166675N0Z', '2021-11-29', '07:00:41', '12:00:41', 0, 1, 0, NULL, NULL, NULL, 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL DEFAULT '0',
  `member_code` varchar(20) DEFAULT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Cp` varchar(11) DEFAULT NULL,
  `Department` varchar(50) DEFAULT NULL,
  `Course` varchar(50) DEFAULT NULL,
  `YearSection` varchar(5) DEFAULT NULL,
  `verified` set('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `verifycode` varchar(7) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` mediumint UNSIGNED DEFAULT NULL,
  `updated_by` mediumint UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive','locked','unlocked','filled','pre-enroll') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'active',
  `archived` tinyint UNSIGNED DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `code`, `member_code`, `Name`, `email`, `Cp`, `Department`, `Course`, `YearSection`, `verified`, `verifycode`, `created_at`, `created_by`, `updated_by`, `status`, `archived`) VALUES
(33, 'BBT1638166837', 'AYQ1638166675N0Z', 'Juan Dela Cruz', 'mvallite@usa.edu.ph', '09506345982', 'CEIT', 'IT', '4A', '1', '881504', '2021-11-29 14:20:37', NULL, NULL, 'active', 0),
(32, 'GFF1637988816', 'BZ21637917423YWM', 'Jun Piloton', 'apiloton@ssct.edu.ph', '09506345982', 'CEIT', 'IT', '4C', '1', '249153', '2021-11-27 12:53:36', NULL, NULL, 'active', 0),
(31, 'GFF1637988816', 'APU16379851149PP', 'Jun Piloton', 'apiloton@ssct.edu.ph', '09506345982', 'CEIT', 'IT', '4C', '1', '249152', '2021-11-27 12:53:36', NULL, NULL, 'active', 0),
(17, 'dKE1637915554', 'BZ21637917423YWM', 'Antonio Piloton', 'antoniollealdeluibpiloton@gmail.com', '09506345982', 'CEIT', 'IT', '4C', '1', '656271', '2021-11-26 16:32:34', NULL, NULL, 'active', 0),
(36, 'RSN1642314307', 'SDM1642311260SGZ', 'HON. THOMAS A. BOCAGO', 'apiloton1@ssct.edu.ph', '09506345982', 'SD', 'dsdsd', '5', '0', '999675', '2022-01-16 14:25:07', NULL, NULL, 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `details` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `area` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dev` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `details`, `area`, `dev`) VALUES
(1, 'event_list', 'Event Management [List]', '', '0'),
(2, 'event_add', 'Event Management [Add]', '', '0'),
(3, 'event_manage', 'Event Management', '', '0'),
(4, 'user_manage', 'User Management', '', '0'),
(5, 'user_list', 'User Management [List]', '', '0'),
(6, 'user_add', 'User Management [Add]', '', '0'),
(7, 'event_delete', 'Event Management [ Delete]', 's', '0'),
(8, 'event_done', 'Event Management [Set Event as Done]', 'a', '0'),
(9, 'event_edit', 'Event Management [Edit]', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `code`, `name`) VALUES
(1, 'ACAD', 'Faculty'),
(2, 'EV', 'HR'),
(3, 'EA', 'Supply'),
(4, 'SEC', 'College Secretary'),
(5, 'SA', 'Super ADministrator'),
(6, 'LI', 'Librarian'),
(7, 'AC', 'Accountant'),
(8, 'DSA', 'Director of Student Affairs'),
(9, 'REG', 'Registrar'),
(10, 'SSG', 'Supreme Student Government'),
(11, 'REGS', 'Registrar Staff'),
(12, 'DEAN', 'Dean'),
(13, 'STUD', 'STUDENT'),
(14, 'PC', 'Program Chair'),
(15, 'GRADE', 'Grade Encoder'),
(16, 'TLR', 'Teller'),
(17, 'SAS', 'Student Account Staff'),
(18, 'ACCTS', 'Accounting Staff'),
(19, 'SFAU', 'SFAU Staff'),
(20, 'SFAC', 'SFAU Coordinator'),
(21, 'CD', 'Campus Director'),
(22, 'VPAA', 'VP Academic Affairs'),
(23, 'VPADM', 'VP Administration'),
(24, 'VPRDE', 'VP RDE'),
(25, 'PRES', 'President'),
(26, 'BDGT', 'Budget Officer'),
(27, 'HRS', 'HR Staff'),
(28, 'LMC', 'Learning Materials Comm');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE IF NOT EXISTS `role_permissions` (
  `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` smallint UNSIGNED NOT NULL,
  `permission_id` smallint UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `payload` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_activity` int NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `verified` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `code` varchar(7) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `verified`, `code`) VALUES
(1, 'Jun Piloton', 'apiloton1@ssct.edu.ph', '$2y$10$iLBroABBJvS0aCiSYvHqnuBQBd/WjkLJVe8.CyCtDepXcyjPymwDy', 1, '0'),
(2, 'Jun Piloton', 'chandy@ssct.edu.ph', '$2y$10$yAqMuAnIIIkDDDAaMYETp.iO8NuROsZWllITR.cTmt1jusYQAleU6', 1, '0'),
(3, 'Antonio Piloton', 'apiloton@ssct.edu.ph', '$2y$10$.DihQM7hLUIXJDfT7mKRK.dFBcF/nTORk81Bh1b8s8dn9bWxif3iK', 1, '0'),
(4, 'Cashier 1', 'apiloto@ssct.edu.ph', '$2y$10$sVxqbc4JGIvenA1bgEpLyuTd70ax.uyyAaZSR6rVlo3wBE4Kz0Tx6', 0, '0'),
(5, 'td_final', 'chandy@sss', '$2y$10$T3xa0zitHh44iIvEk3EhauXyWusIlblGHHoXFLkdmYBX/qYIoFKA2', 0, '0'),
(6, 'Sample', 'jhambre@ssct.edu.ph', '$2y$10$6CXfwOj96kAIMQV7p3KX.uM2TXmisYXCqLOE7g2a13sESZrD2fDbS', 0, '0'),
(7, 'Master Dev', 'dev@ssct.edu.ph', '$2y$10$iLBroABBJvS0aCiSYvHqnuBQBd/WjkLJVe8.CyCtDepXcyjPymwDy', 1, '0'),
(8, 'SAMPLE NEW RES', 'antoniollealdeluibpiloton@usa.edu.ph', '$2y$10$o3VfSV5KhYRhiLkIjrQPpeirZl0QwbFG5SkcnIna5jupb19Wu0.RO', 0, 'TKOQCSY');

-- --------------------------------------------------------

--
-- Table structure for table `userss`
--

DROP TABLE IF EXISTS `userss`;
CREATE TABLE IF NOT EXISTS `userss` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE IF NOT EXISTS `user_permissions` (
  `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `permission_id` smallint UNSIGNED NOT NULL,
  `exclude` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `permission_id`, `exclude`) VALUES
(27, 3, 3, 0),
(49, 7, 3, 0),
(50, 7, 7, 0),
(51, 7, 2, 0),
(52, 7, 9, 0),
(53, 7, 1, 0),
(54, 7, 8, 0),
(55, 7, 4, 0),
(56, 7, 6, 0),
(57, 7, 5, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
