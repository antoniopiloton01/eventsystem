var map;
var mapData;
var markers=[];
var gender
var mapView='marker'

function initMap() {
  if(!map) {
    //center at caraga region
    var latlng = new google.maps.LatLng(9.0842869,125.9142393);
    
      map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 8,
          gestureHandling: 'cooperative',
          scrollwheel: true,
          streetViewControl: false,
          mapTypeIds: ['roadmap', 'terrain','satellite','hybrid']
      });

    var b1 = document.getElementById('btnMarkerImage');
    var b2 = document.getElementById('btnDotImage');
    var b3 = document.getElementById('btnTotalsImage');
    var b4 = document.getElementById('btnFemaleImage');
    var b5 = document.getElementById('btnMaleImage');

    map.controls[google.maps.ControlPosition.RIGHT].push(b1);
    map.controls[google.maps.ControlPosition.RIGHT].push(b2);
    map.controls[google.maps.ControlPosition.RIGHT].push(b3);
    map.controls[google.maps.ControlPosition.RIGHT].push(b4);
    map.controls[google.maps.ControlPosition.RIGHT].push(b5);


      getData('enrolled_students')

  }
}

function getData(type){
    var formData=new FormData()
    formData.append('type',type)
    $.ajax({
        type: "POST",
        url: baseUrl+'/map/data',
        data: formData,
        dataType: 'json',
        processData:false,
        contentType:false,
        cache:false,
        success:function(data){
          mapData=data.data
          loadMap()
          loadList()
          loadChart1()
        }
    });
}
function loadList(){
    var h=''
    var md=mapData
    for(var i in md){
        var d=md[i]
        cD=''
        pD=''
        if(d.citymunDesc!=null) cD = d.citymunDesc
        if(d.provDesc!=null) pD = d.provDesc
        if(pD!='') pD=", "+pD
        pD=cD+pD
        h+='<li class="list-group-item  justify-content-between align-items-center">'
            h+='<b>'+pD+'</b>'
          h+='<div class="row">'
            h+='<div class="col-md-4 text-success font-weight-bold">'
              h+='<i class="mdi mdi-human-female"></i>'+(d.female!==undefined?d.female:0)
            h+='</div>'
            h+='<div class="col-md-4 text-info font-weight-bold">'
              h+='<i class="mdi mdi-human-male"></i>'+(d.male!==undefined?d.male:0)
            h+='</div>'
            h+='<div class="col-md-4 text-info font-weight-bold">'
              h+='<i class="mdi mdi-human-male-female"></i>'+d.num
            h+='</div>'
          h+='</div>'
        h+='</li>'
    }
    $('#list').html(h)
}

function loadMarkers(type){
    iconUrl='https://covid19.ssct.edu.ph/images/red-blank2.png'
    if(gender=='male') iconUrl='https://covid19.ssct.edu.ph/images/blu-blank2.png'
    if(gender=='female') iconUrl='https://covid19.ssct.edu.ph/images/pink-blank2.png'
    for(var i in mapData){
        var d=mapData[i]
        var num=0
        if(gender=='male'){
          num=d.male
        }else if(gender=='female'){
          num=d.female
        }else{
          num=d.num
        }
        if(d.lat!==null && d.long!==null && num>0){
            var l={lat: parseFloat(d.lat), lng: parseFloat(d.long)}
            var marker=new google.maps.Marker({
                position: l,
                map: map,
                label:num.toString(),
                icon:iconUrl
            });
            markers.push(marker)

            var infoWindow = new google.maps.InfoWindow;

            content=getIconTip(d)

            google.maps.event.addListener(marker,'click', (function(marker,content,infoWindow){ 
              return function() {
              infoWindow.setContent(content);
              infoWindow.open(map,marker);
              };
            })(marker,content,infoWindow));

        }
    }
}

function getIconTip(d){
  var infowincontent = document.createElement('div');
  var strong = document.createElement('strong');

  var description=d.citymunDesc+', '+d.provDesc
  strong.textContent = description
  infowincontent.appendChild(strong);

  infowincontent.appendChild(document.createElement('br'));
  infowincontent.appendChild(document.createElement('br'));

  text = document.createElement('text');
  text.textContent = 'Total Number of Students: '
  infowincontent.appendChild(text);
  strong = document.createElement('strong');
  strong.textContent = d.num
  infowincontent.appendChild(strong);

  infowincontent.appendChild(document.createElement('br'));
  text = document.createElement('text');
  text.textContent = 'Male: '
  infowincontent.appendChild(text);
  strong = document.createElement('strong');
  strong.textContent = d.male
  infowincontent.appendChild(strong);

  infowincontent.appendChild(document.createElement('br'));
  text = document.createElement('text');
  text.textContent = 'Female: '
  infowincontent.appendChild(text);
  strong = document.createElement('strong');
  strong.textContent = d.female
  infowincontent.appendChild(strong);

  infowincontent.appendChild(document.createElement('br'));
  infowincontent.appendChild(document.createElement('br'));
  button = document.createElement('button');
  button.setAttribute("class", "btn btn-sm btn-primary");
  button.innerHTML="View Students"
  button.onclick=function(){
      viewStudents(d.hcity)
  }
  infowincontent.appendChild(button);

  return infowincontent;
}

function createStudentListTable(){
  $('#studentsListDiv').html('')
  var h= '<table id="studentsList" width="100%"  class="cell-border stripe">'
      h+='<thead>'
      h+='<th>Student ID</th>'
      h+='<th>Name</th>'
      h+='<th>Curriculum</th>'
      h+='<th>Contact Number</th>'
      h+='<th>Contact Person</th>'
      h+='<th>Address</th>'
      h+='</thead>'
      h+='<tbody></tbody>'
      h+='</table>'
  $('#studentsListDiv').html(h)
}

function viewStudents(city){
    createStudentListTable()
    var formData=new FormData()
    formData.append('city',city)
    if($('#CurriculumID').val()>0) formData.append('CurriculumID',$('#CurriculumID').val())
    if($('#YearLevel').val()>0) formData.append('YearLevel',$('#YearLevel').val())
    $.ajax({
        type: "POST",
        url: baseUrl+'/map/students',
        data: formData,
        dataType: 'json',
        processData:false,
        contentType:false,
        cache:false,
        success:function(data){
            
            var h=''
            for(var i in data.students){
                var d=data.students[i]
                var ne=''
                if(d.nameExtension!=null) ne=' '+d.nameExtension
                h+='<tr>'
                h+='<td>'+d.StudentYear+'-'+pad(d.StudentID2,5)+'</td>'
                h+='<td>'+d.lastName+','+d.firstName+' '+d.middleName+ne+'</td>'
                h+='<td>'+d.ProgramCode+'-'+d.YearLevel+'</td>'
                h+='<td>'+d.PhoneNumber+'</td>'
                h+='<td>'+d.contactPerson+'</td>'
                h+='<td>'+d.Address+'</td>'
                h+='</tr>'
            }
            $('#studentsList tbody').html(h)

            $('#studentsModal').modal('show');

              var table=$('#studentsList').DataTable({
              "responsive":true,
              "processing": true,
              "order": [[ 0, "asc" ]],
              "columnDefs": [ {
                "targets": [0,1,2,3,4,5],
                "orderable": true,
                "searchable": true,
                }
              ],
              "sPaginationType": "full_numbers",
              "minimumResultsForSearch": "3",
              "lengthMenu": [[25, 50,100,500],[25,50,100,500]],
              'dom': 'Blfrtip',
              'buttons': [
                  {
                      extend: 'copyHtml5',
                      titleAttr: 'Copy'
                  },
                  /*{
                      extend: 'print',
                      text: '<i class="fa fa-print" aria-hidden="true"></i>',
                      titleAttr: 'Print'
                      //autoPrint: true
                  },{
                      extend: 'pdfHtml5',
                      text: '<i class="fa fa-file" aria-hidden="true"></i>',
                      titleAttr: 'PDF'
                  },*/
                  {
                      extend: 'csvHtml5',
                      // text: '<i class="fa fa-download" aria-hidden="true"></i>',
                      titleAttr: 'CSV'
                  },
              ],
            });
        }
    });
}

function loadChart1(){
  var sdata=[]
  for(var i in mapData){
      var d=mapData[i]
      var cD=''
      var pD=''
      if(d.citymunDesc!=null) cD = d.citymunDesc
      if(d.provDesc!=null) pD = d.provDesc
      if(pD!='') pD=", "+pD
      pD=cD+pD
      sdata.push({name:pD , y:parseFloat(d.num)})
  }

Highcharts.chart('city-mun-chart', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
    enabled: false
    },
    title: {
        text: 'Students by Location'
    },
    tooltip: {
        pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: sdata
    }]
});


}

$('#btnTotal').on('click',function(){
    clearMarkers()
    gender='totals'
    loadMap()
    btnCases(this)
});

$('#btnMale').on('click',function(){
    clearMarkers()
    gender='male'
    loadMap()
    btnCases(this)
});

$('#btnFemale').on('click',function(){
    clearMarkers()
    gender='female'
    loadMap()
    btnCases(this)
});

$('#btnTotalsImage').on('click',function(){
    clearMarkers()
    gender='totals'
    loadMap()
    //btnCases(this)
});

$('#btnMaleImage').on('click',function(){
    clearMarkers()
    gender='male'
    loadMap()
    //btnCases(this)
});

$('#btnFemaleImage').on('click',function(){
    clearMarkers()
    gender='female'
    loadMap()
    //btnCases(this)
});

function loadMap(){
  if(mapView=='marker') loadMarkers()
  if(mapView=='dot') loadDots()
}


function clearMarkers(){
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
}

function loadDots(type){
  for(var k in mapData){    
    var d=mapData[k]
    var numCases=d.num
    if(d.lat!==null && d.long!==null & numCases>0){
      var color='#FF0000'     
      var scale=.2
      var fill
      var num=0
      if(gender=='male'){
        num=d.male
      }else if(gender=='female'){
        num=d.female
      }else{
        num=d.num
      }

      if(gender=='female'){
        color='#f66d9b'
      }else if(gender=='male'){
        color='#45aaf2'
      }else {
        color='#ff3d3d'
      }
        
      if(num>0 && num<=10){
        scale=.2
        fill=1
      }else if(num>10 && num<=20){
        scale=.4
        fill=1
      }else if(num>20 && num<=40){
        scale=.6
        fill=.9
      }else if(num>40 && num<=100){
        scale=.8
        fill=.8
      }else if(num>100 && num<=500){
        scale=1
        fill=.7
      }else if(num>500 && num<=1200){
        scale=1.1
        fill=.6
      }else if(num>1200 && num<=1500){
        scale=1.25
        fill=.5
      }else if(num>1500){
        scale=1.5
        fill=.4
      }

      var icon = {        
            path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
            fillColor: color,
            fillOpacity: fill,
            anchor: new google.maps.Point(0,0),
            strokeWeight: .5,
            scale: scale
        }

      var l={lat: parseFloat(d.lat), lng: parseFloat(d.long)}
      marker=new google.maps.Marker({
        position: l,
        map: map,
        icon:icon
      });
      
      var infoWindow = new google.maps.InfoWindow;

      content=getIconTip(d)

      google.maps.event.addListener(marker,'click', (function(marker,content,infoWindow){ 
        return function() {
        infoWindow.setContent(content);
        infoWindow.open(map,marker);
        };
      })(marker,content,infoWindow));

      markers.push(marker)
    }
  }
}

$('#btnDots').on('click',function(){
  clearMarkers()
  mapView='dot'
  loadDots()
  btnTypes(this)
})

$('#btnDotImage').on('click',function(){
  clearMarkers()
  mapView='dot'
  loadDots()
  //btnTypes(this)
})

$('#btnMarkers').on('click',function(){
  clearMarkers()
  mapView='marker'
  loadMarkers()
  btnTypes(this)
})

$('#btnMarkerImage').on('click',function(){
  clearMarkers()
  mapView='marker'
  loadMarkers()
  //btnTypes(this)
})

function btnTypes(e){
  $('.btnType').css('background-color','#337ab7');
  $('.btnType').css('border-color','#2e6da4');

  $(e).css('background-color','#d9534f');
  $(e).css('border-color','#d43f3a');
}

function btnCases(e){
  $('.btnCase').css('background-color','#337ab7');
  $('.btnCase').css('border-color','#2e6da4');

  $(e).css('background-color','#d9534f');
  $(e).css('border-color','#d43f3a');
}


$('#CurriculumID').select2({
  ajax: {
    // url: "{{ route('x_getprogNameProgCodeFull')}}",
    url: x_getCurrentCurriculumCourseID,
    dataType: 'json',
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    processResults: function(data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
      return {
        results: data.items
      };
    }
  }
});

function getStudents(){
  var formData=new FormData()
  formData.append('CurriculumID',$('#CurriculumID').val())
  formData.append('YearLevel',$('#YearLevel').val())
  formData.append('type','by_curriculum')
  $.ajax({
    type: "POST",
    url: baseUrl+'/map/data',
    data: formData,
    dataType: 'json',
    processData:false,
    contentType:false,
    cache:false,
    success:function(data){
      if(data.result==true && data.data!==undefined){
        clearMarkers()
        mapData=data.data
        loadMap()
        loadList()
        loadChart1()

      }else{
        PnotifyBox("Notice","notice","No record found for the current term.");
      }
    },
    error:function(data){
      PnotifyBox("Error!","error","Unable to process your request");
    }
  });
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}