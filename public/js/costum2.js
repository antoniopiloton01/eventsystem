$(document).ready(function() {
    $('html,body').animate({ scrollTop: 0 }, 800);
});

function ScrollTop() {
    $('html,body').animate({ scrollTop: 0 }, 800);
}

var btn = $('#buttonTop');

$(window).scroll(function() {
    if ($(window).scrollTop() > 50) {
        $(".top_nav").addClass('scroll')
        btn.addClass('show');
    } else {
        btn.removeClass('show');
        $(".top_nav").removeClass('scroll')
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});


function loaderOut() {
    // setTimeout(function(){
    $("#loader").hide()
        // },1200)
}

function loaderIn() {
    $("#loader").show()
}


$(document).ready(function() {
    loaderOut()
})