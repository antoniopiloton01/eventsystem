<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserRoles;
use App\RolePermissions;
use App\Hris\UserPermissions;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_token','user_type','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table ='users';

    public function permissions(){
        return $this->hasMany('App\Permissions','Permissions');
    }

    public static function getUserAllPermissions($userID,$returnPCodeAsKey=false,$employeeID=0){
        if($userID>0){
            $rpermissions=[];
            
            //get user permissions
            $upermissions=UserPermissions::leftJoin('permissions as p','user_permissions.permission_id','=','p.id')
                            ->where('user_permissions.user_id','=',$userID)
                            ->select('p.*','user_permissions.exclude')
                            ->distinct()->get();
            $upermissions=$upermissions->toArray();
            $excludedPermissions=[];
            $allowedPermissions=[];
            if(count($upermissions)>0){
                foreach($upermissions as $p){
                    if($p['exclude']==1){
                        $excludedPermissions[]=$p;
                    }else{
                        $allowedPermissions[]=$p;
                    }
                }
            }
            $permissions=[];
            if(count($rpermissions)>0){
                $permissions=array_merge($rpermissions,$allowedPermissions);
            }else{
                $permissions=$allowedPermissions;
            }

            if(count($excludedPermissions)>0){
                foreach($permissions as $k=>$p){
                    foreach($excludedPermissions as $e){
                        if($p['id']==$e['id']){
                            unset($permissions[$k]);
                            break;
                        }
                    }
                }
            }
            if(count($permissions)>0){
                //recreate permissions with id as its key
                $temp=[];
                foreach($permissions as $p){
                    if(!array_key_exists($p['id'],$temp)){
                        if($returnPCodeAsKey){
                            $temp[$p['code']]=$p;
                        }else{
                            $temp[$p['id']]=$p;
                        }
                    }
                }
                return $temp;
            }
        }
        return false;
    }

    public static function getUserDetails($userID){
        if($userID>0){
            $res=User::select('*')->where('id','=',$userID)->where('verified','=',1)->limit(1)->get();
            if(count($res)>0){
                return $res->first();
            }
        }
        return false;
    }

    public static function verified($userID){
        if($userID>0){
            $res=User::select('*')->where('id','=',$userID)->where('verified','=',1)->limit(1)->get();
            if(count($res)>0) return true;
        }
        return false;
    }

    public static function verifiedEmail($email){
        if(!empty($email)){
            $res=User::select('*')->where('email','=',$email)->where('verified','=',1)->limit(1)->get();
            if(count($res)>0) return true;
        }
        return false;
    }

    public static function getEmployeeUserID($employeeID){
        if($employeeID>0){
            $res=User::select('id')->where('conn_id','=',$employeeID)->where('user_type','=',1)->first();
            if($res) return $res->id;
        }
        return false;
    }
}
