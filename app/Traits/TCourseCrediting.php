<?php

namespace App\Traits;

use App\Es\Enrollees;
use App\Es\StudentCreditedCourses;
use App\Es\xGrades;
use App\User;

trait TCourseCrediting
{
    public function checkCreditCourseGetGrade(&$grade, $studentID, $curriculumCourseID)
    {
        //check grade if its credited then use the credited grade from external course
        $credit = StudentCreditedCourses::getCreditedCourseEnrollmentGrade($studentID, $curriculumCourseID);

        if ($credit) {
            if ($credit['CreditType'] == 1) {
                $xg = xGrades::getGradeByID($studentID, $credit->CourseID);
                if ($xg) {
                    if (! empty($xg->Completed) && $xg->Completed > 0) {
                        $grade = number_format($xg->Completed, 1);
                    } else {
                        if ($xg->Grade == 'DR' || $xg->Grade == 'Dro' || $xg->Grade == 'DROP' || $xg->Grade == 'drp' || $xg->Grade == 'Drp') {
                            $grade = 'DROP';
                        } else {
                            $grade = (strlen($xg->Grade) > 1) ? number_format((float) $xg->Grade, 1) : '';
                        }
                    }

                    return true;
                }
            } else {
                //get grade
                $xg = Enrollees::select('Grade', 'Completed')->where('StudentID', '=', $studentID)->where('CurriculumCoursesID', '=', $credit->CourseID)->first();
                if ($xg) {
                    if (! empty($xg->Completed) && $xg->Completed > 0) {
                        $grade = number_format($xg->Completed, 1);
                    } else {
                        if ($xg->Grade == 'DR' || $xg->Grade == 'Dro' || $xg->Grade == 'DROP' || $xg->Grade == 'drp' || $xg->Grade == 'Drp') {
                            $grade = 'DROP';
                        } else {
                            $grade = (strlen($xg->Grade) > 1) ? number_format((float) $xg->Grade, 1) : '';
                        }
                    }

                    return true;
                }
            }
        }

        return false;
    }
}
