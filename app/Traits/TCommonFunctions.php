<?php

namespace App\Traits;

use App\Es\Students;
use App\Hris\Employees;
use App\MessageBoards;
use App\User;
use Illuminate\Support\Facades\Auth;

trait TCommonFunctions
{
    public function setCommonFields(&$object, $hasArchived = true, $hasStatus = true)
    {
        if (is_object($object)) {
            $object->created_by = Auth::user()->id;
            $object->updated_by = Auth::user()->id;
            $object->created_at = date('Y-m-d H:i:s');
            if ($hasStatus) {
                $object->status = 'active';
            }

            if ($hasArchived) {
                $object->archived = 0;
            }
        } elseif (is_array($object)) {
            $object['created_by'] = Auth::user()->id;
            $object['updated_by'] = Auth::user()->id;
            $object['created_at'] = date('Y-m-d H:i:s');
            if ($hasArchived) {
                $object['archived'] = 0;
            }

            if ($hasStatus) {
                $object['status'] = 0;
            }
        }
    }

    public function setUpdateFields(&$object)
    {
        if (is_object($object)) {
            $object->updated_by = Auth::user()->id;
        }
        if (is_array($object)) {
            $object['updated_by'] = Auth::user()->id;
        }
    }

    public function setYearLevelSuffix($year)
    {
        switch ($year) {
            case 1:
                $year .= 'st';
                break;
            case 2:
                $year .= 'nd';
                break;
            case 3:
                $year .= 'rd';
                break;
            case 4:
            case 5:
                $year .= 'th';
                break;
            default:
                $year .= '';
                break;
        }

        return $year;
    }

    public function setYearLevelName($year)
    {
        switch ($year) {
            case 1:
                $year = 'First Year';
                break;
            case 2:
                $year = 'Second Year';
                break;
            case 3:
                $year = 'Third Year';
                break;
            case 4:
                $year = 'Fourth Year';
                break;
            case 5:
                $year = 'Fifth Year';
                break;
            case 7:
                $year = 'G-12';
                break;
            default:
                $year = '';
                break;
        }

        return $year;
    }

    public function setSemesterName($num)
    {
        switch ($num) {
            case 1:
                $num = '1st Semester';
                break;
            case 2:
                $num = '2nd Semester';
                break;
            case 3:
                $num = 'Summer';
                break;
            case 4:
                $num = '2nd Summer';
                break;
            case 5:
                $num = '3rd Semester';
                break;
            default:
                $num = '';
                break;
        }

        return $num;
    }

    public function setShortSemesterName($num)
    {
        switch ($num) {
            case 1:
                $num = '1st Sem';
                break;
            case 2:
                $num = '2nd Sem';
                break;
            case 3:
                $num = 'Summer';
                break;
            case 4:
                $num = '2nd Summer';
                break;
            case 5:
                $num = '3rd Sem';
                break;
            default:
                $num = '';
                break;
        }

        return $num;
    }

    public function setSemesterSuffix($num)
    {
        switch ($num) {
            case 1:
                $num = '1st Semester';
                break;
            case 2:
                $num = '2nd Semester';
                break;
            case 3:
                $num = 'Summer';
                break;
            case 4:
                $num = '2nd Summer';
                break;
            case 2:
                $num = '3rd Semester';
                break;
            default:
                $num = '';
                break;
        }

        return $num;
    }

    public function echolog($message = '')
    {
        echo strtotime('now').': '.$message."\n";
    }

    public function setTerm(&$request, &$o)
    {
        $o->semester = $request->session()->get('semester');
        $o->ayFrom = $request->session()->get('termFrom');
        $o->ayTo = $request->session()->get('termTo');
        $o->data['ayFrom'] = $o->ayFrom;
        $o->data['ayTo'] = $o->ayTo;
        $o->data['semester'] = $o->semester;
        $o->data['semesterLabel'] = $this->setSemesterSuffix($o->semester);
        $o->data['noTermSelectionRoutes'] = ['studentgrades', 'gradeEncoding', 'studentevaluation'];
    }

    /** TBD **/
    public function encryptClassCode($n)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $hash = '';
        $textToEncrypt = 2853;
        $secretHash = 'ABCDEFGHIJIKLMN';
        $encryptionMethod = 'crc32';
        $d = openssl_encrypt($textToEncrypt, $encryptionMethod, $secretHash);
        echo var_dump($d);
        $e = openssl_decrypt($d, $encryptionMethod, $secretHash);
        echo var_dump($e);

        return $hash;
    }

    public function decryptClassCode($n)
    {
        $hash = '';

        return $hash;
    }

    public function setSchoolGrade($grade)
    {
        $g = (int) $grade;
        switch ($g) {
            case 6:
                $grade = 'DRP';
                break;
            case 7:
                $grade = 'INC';
                break;
            default:
                break;
        }

        return $grade;
    }

    public function setEmployee(&$request, &$o)
    {
        $e = User::where('id', $o->userID)->first();
        if ($e) {
            $o->employeeID = $e->conn_id;
        }
    }

    public function setStudent(&$request, &$o)
    {
        $e = User::where('id', $o->userID)->first();
        if ($e) {
            $o->studentID = $e->conn_id;
        }
    }

    public function randomString($length = 16)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    public function checkRegenerateUserEmail($firstName, $lastName)
    {
        if (! empty($firstName) && ! empty($lastName)) {
            $i = 0;
            $firstName = strtolower($firstName);
            $lastName = strtolower($lastName);
            $this->cleanUserName($firstName);
            $this->cleanUserName($lastName);
            $uname = $firstName[0].$lastName.'@ssct.edu.ph';
            while ($i >= 0) {
                if ($i > 0) {
                    $uname = $firstName[0].$lastName.$i.'@ssct.edu.ph';
                }
                $exists = User::where('email', $uname)->exists();
                if ($exists == false) {
                    break;
                }

                $i++;
            }

            return $uname;
        }

        return $this->randomString(16).'@ssct.edu.ph';
    }

    private function cleanUserName(&$uname)
    {
        $uname = preg_replace(
            ['/Ñ/', '/ñ/', '/ /', '/"/', "/'/"],
            ['n', 'n', '', '', ''],
            $uname
        );
        $uname = preg_replace('/[^a-zA-Z0-9]+/', '', $uname);
        $uname = filter_var($uname, FILTER_SANITIZE_STRING);
    }

    public function checkNstp($ccode)
    {
        if (strpos(strtolower($ccode), 'nstp') !== false) {
            return true;
        }

        return false;
    }

    public function getMessageBoards(&$data, $user)
    {
        $unitID = Employees::getEmployeeUnitID($user);
        $areas = [35 => 'registrar', 21 => 'cashier', 27 => 'hr', 4 => 'ceit'];
        if (isset($areas[$unitID])) {
            $ms = MessageBoards::distinct()->select('message')->where('area', '=', $areas[$unitID])->orderBy('created_at', 'desc')->orderBy('updated_at', 'desc')->first();
            if (isset($ms->message) && ! empty($ms->message)) {
                $data[$areas[$unitID].'Message'] = $ms->message;
            }
        }
        $all = MessageBoards::select('message')->where('area', '=', 'all')->where('message', '<>', '')->first();
        if ($all && isset($all->message) && ! empty($all->message)) {
            $data['allMessage'] = $all->message;
        }
    }

    public function formatStudentName(&$d)
    {
        if (is_object($d)) {
            return $d->LastName.', '.$d->FirstName.' '.$d->MiddleName[0].'. '.$d->NameExtension;
        } else {
            return $d['LastName'].', '.$d['FirstName'].' '.$d['MiddleName'][0].'. '.$d['NameExtension'];
        }
    }

    public function formatStudentID(&$d)
    {
        if (is_object($d)) {
            return $d->LastName.'-'.str_pad('0', 5, $d->StudentID2, STR_PAD_LEFT);
        } else {
            return $d['StudentYear'].'-'.str_pad('0', 5, $d['StudentID2'], STR_PAD_LEFT);
        }
    }

    public function selectStudents($postData)
    {
        $query = $postData['_type'];
        $data = [];
        $filter = isset($postData['q']) ? $postData['q'] : '';
        if ($query == 'query' && empty($postData['q'])) {
            $res = Students::distinct()->select('students.*')
            ->leftJoin('student_curriculums', 'student_curriculums.StudentID', '=', 'students.StudentID')
            ->where('students.archived', '=', 0)
            ->where('student_curriculums.ayFrom', '=', $this->ayFrom)
            ->where('student_curriculums.ayTo', '=', $this->ayTo)
            ->where('student_curriculums.Semester', '=', $this->semester)
            ->inRandomOrder()->limit(20)->get();
            foreach ($res as $r) {
                $studentCode = str_pad($r->StudentID2, 5, '0', STR_PAD_LEFT);
                $studentID = $r->StudentYear.'-'.$studentCode;
                $data[] = ['id' => encrypt($r->StudentID), 'text' => '['.$studentID.'] '.$r->LastName.', '.$r->FirstName.' '.$r->MiddleName];
            }
        } else {
            $res = Students::distinct()->select('students.*')
                ->leftJoin('student_curriculums', 'students.StudentID', '=', 'student_curriculums.StudentID')
                ->where('student_curriculums.ayFrom', '=', $this->ayFrom)
                ->where('student_curriculums.ayTo', '=', $this->ayTo)
                ->where('student_curriculums.Semester', '=', $this->semester)
                ->where('students.archived', '=', 0);
            if (strpos($filter, '-')) {
                $t = explode('-', $postData['q']);
                $studentYear = isset($t[0]) ? $t[0] : '';
                $studentID2 = isset($t[1]) ? $t[1] : '';
                if (strlen($studentYear) == 4) {
                    $res->where('StudentYear', 'like', '%'.$studentYear.'%');
                }
                if (strlen($studentID2) > 0) {
                    $sid2 = strval((int) $studentID2);
                    $res->where('StudentID2', 'like', '%'.$sid2.'%');
                }
            } elseif (strpos($filter, ' ')) {
                $t = explode(' ', $postData['q']);
                $name_one = isset($t[0]) ? $t[0] : '';
                $name_two = isset($t[1]) ? $t[1] : '';
                $res->where('FirstName', 'like', '%'.$name_one.'%');
                $res->where('LastName', 'like', '%'.$name_two.'%');
                $res->orWhere('FirstName', 'like', '%'.$name_two.'%');
                $res->where('LastName', 'like', '%'.$name_one.'%');
            } else {
                $res->where(function ($res) use ($filter) {
                    if (! empty($filter)) {
                        $res->orWhere('FirstName', 'like', '%'.$filter.'%');
                        $res->orWhere('LastName', 'like', '%'.$filter.'%');
                        $res->orWhere('students.StudentID', '=', $filter);
                        $res->orWhere('StudentYear', 'like', '%'.$filter.'%');
                    }
                });
            }
            $res = $res->orderBy('Lastname')
                ->orderBy('Firstname')
                ->limit(20);
            $res = $res->get();
            foreach ($res as $r) {
                $studentCode = str_pad($r->StudentID2, 5, '0', STR_PAD_LEFT);
                $studentID = $r->StudentYear.'-'.$studentCode;
                $data[] = ['id' => encrypt($r->StudentID), 'text' => '['.$studentID.'] '.$r->LastName.', '.$r->FirstName.' '.$r->MiddleName];
            }
        }

        return $data;
    }

    public function validateGradeDisplay($grade)
    {
        if (preg_match('/[A-Za-z]/', $grade)) {
            $grade = strtolower($grade);
        }
        if (is_string($grade)) {
            $grade = strtolower($grade);
        }

        switch ($grade) {
            case 'drop':
            case 'drp':
            case '6':
            case 6:
                $grade = 'DRP';
                break;
            case 'incomplete':
            case 'inc':
            case '7':
            case 7:
                $grade = 'INC';
                break;
            case 11:
                $grade = 'PASSED';
                break;
            case '11':
                $grade = 'PASSED';
                break;
            case 'NG':
            case null:
                $grade = '';
                break;
            default:
                $grade = $grade;
        }

        return $grade;
    }

    private function similarity($str1, $str2)
    {
        $len1 = strlen($str1);
        $len2 = strlen($str2);

        $max = max($len1, $len2);
        $similarity = $i = $j = 0;

        while (($i < $len1) && isset($str2[$j])) {
            if (isset($str1[$i]) && isset($str2[$j]) && $str1[$i] == $str2[$j]) {
                $similarity++;
                $i++;
                $j++;
            } elseif ($len1 < $len2) {
                $len1++;
                $j++;
            } elseif ($len1 > $len2) {
                $i++;
                $len1--;
            } else {
                $i++;
                $j++;
            }
        }

        return (round($similarity / $max, 2)) * 100;
    }

    //convert all inputted grades
    public function checkgrade($grade)
    {
        switch ($grade) {
            case 'inc':
            case 'INC':
            case '7': $grades = 'INC'; break;
            case 'drp':
            case 'DRP':
            case 'DROPPED':
            case 'dropped':
            case 'drop':
            case 'DROP':
            case '6': $grades = 'DRP'; break;
            case 'passed':
            case 'PASSED':
            case 'PASS':
            case 'pass':
            case '11': $grades = 'PASSED'; break;
            case '':
            case null: $grades = ''; break;
            default:
                $grades = $grade;
                break;
        }

        return $grades;
    }
}
