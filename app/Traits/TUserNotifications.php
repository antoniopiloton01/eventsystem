<?php

namespace App\Traits;

use App\User;
use App\UserMessages;
use App\UserNotifications;

trait TUserNotifications
{
    public function notifyUser($userID, $message, $link = '', $createdBy = 0)
    {
        if (empty($link)) {
            $link = route('tickets');
        }
        if ($userID >= 0 && ! empty($message)) {
            $un = new UserNotifications();
            $un->created_for = $userID;
            $un->message = $message;
            $un->link = $link;
            $this->setCommonFields($un, false, false);
            $un->status = 'new';
            $un->created_by = $createdBy;

            return $un->save() ? true : false;
        }
    }

    public function getUserNotifications($userID, $read = false)
    {
        $res = UserNotifications::getUserNotifications($userID, $read);
        if ($res) {
            foreach ($res as &$r) {
                $r->eid = encrypt($r->id);
            }

            return $res;
        }
    }

    public function getUserMessages($userID, $read = false)
    {
        $res = UserMessages::getUserMessagesDistinctFrom($userID, $read);
        if ($res) {
            foreach ($res as &$r) {
                $r->eid = encrypt($r->id);
            }

            return $res;
        }
    }
}
