<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Input;

use App\Traits\TPermissions;
use App\User;
use App\Role;
use App\Hris\Permissions;
use App\Hris\UserPermissions;
use App\RolePermissions;
use App\Traits\TCommonFunctions;

class UserController extends Controller
{
  use AuthenticatesUsers, TPermissions, TCommonFunctions;

  private $data = [];
  private $permissions;

    private $ayFrom;
    private $ayTo;
    private $semester;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
      $this->middleware('auth');
      $this->middleware('csrf');
      $this->middleware(function ($request, $next) {
        $this->userID = Auth::user()->id;
        $this->permissions = $this->getPermissions($this->data, true);
        $this->setTerm($request,$this);
        return $next($request);
      });
      $this->middleware('csrf');
    }

    public function addUsers(){
      if(isset($this->permissions['user_add'])){
        return view('auth.register', $this->data);
      }else{
        return view('nopermission');
      }
    }

    public function changepassword(Request $request)
    {
      $this->data['error'] = ['message' => '', 'type' => 'error'];
      if ($request->method() == 'POST') {
            //check if user has the same login password
        $validationErrorMessages = [];
        $rules = ['currentPassword' => 'required|min:6',
        'password' => 'required|confirmed|min:6', ];
        $r = $this->validate($request, $rules, $validationErrorMessages);

        if (isset($r['password'])) {
                //check current password
          $login = Auth::attempt(['email' => Auth::user()->email, 'password' => $r['currentPassword'], 'verified' => 1, 'locked' => 0]);

          if ($login) {
            $password = Hash::make($r['password']);
            Auth::user()->password = $password;
            Auth::user()->save();
            $this->guard()->login(Auth::user());

            $this->data['type'] = 'success';
            $this->data['message'] = 'You have successfully changed your password.';

          } else {
            $this->data['type'] = 'danger';
            $this->data['message'] = 'Invalid current password. Please try again.';
          }
        } else {
          $this->data['message'] = 'Invalid';
        }
      }
      return view('user.changepassword', $this->data);
    }

    
    public function userssss(){
      //get list of all users
      // if(isset($this->permissions['user_view_all'])){
        //exclude students in user management
        $users=User::where('user_type','<','1')->get();

        $this->data['users']=$users;
        return view('user.userslist', $this->data);
      // }
    }

    public function userpermissions(Request $request){
      // if(isset($this->permissions['user_manage_permissions'])){
        //get employee ID
        $id=decrypt($request->segment(3));
        $user=User::where('id','=',$id)->first();
        if(!is_null($user)){
          $this->data['user']=$user;
          //get all permissions
          $cp=Permissions::getAllPermissions();
          $up=UserPermissions::getUserPermissions($user->id);          
          foreach($cp as $k=>$p){
            $cp[$k]->selected=false;
            foreach($up as $t){
              if($p->code==$t['code']) $cp[$k]->selected=true;
            }
          }
          $this->data['cpermissions']=$cp;
          return view('main.users.userpermissions', $this->data);
        }else{
        	return view('enrollment.blank',['message'=>"User don't have an account yet."]);
        }
      // }
      // return view('enrollment.blank',['message'=>"You dont have permission."]);
    }

    public function specialUserPermissions(Request $request){
      if(isset($this->permissions['user_manage_permissions'])){
        //get employee ID
        $userID=decrypt($request->segment(4));
        $user=User::where('id','=',$userID)->first();
        if(!is_null($user)){
          $this->data['user']=$user;
          //get all permissions
          $cp=Permissions::getAllPermissions();
          $up=UserPermissions::getUserPermissions($user->id);          
          foreach($cp as $k=>$p){
            $cp[$k]->selected=false;
            foreach($up as $t){
              if($p->code==$t['code']) $cp[$k]->selected=true;
            }
          }
          $this->data['cpermissions']=$cp;
          return view('user.userpermissions', $this->data);
        }else{
          echo 123;
        }
      }
      return view('enrollment.blank',['message'=>"You dont have permission."]);
    }

    public function x_saveUserPermissions(Request $request){
      // if(isset($this->permissions['user_manage_permissions'])){
        $userID=decrypt($request->segment(3));
        $user=User::where('id','=',$userID)->first();
        if($user){
          $result=['result'=>false,'message'=>"Unable to process request"];
          // $postData=Input::except(['_token']);
          if(isset($request['selectedPermissions']) && !empty($request['userPermissions'])){
            $sp=explode(',',$request['selectedPermissions']);
            if(count($sp)>0){
              $temp=[];
              foreach($sp as $p) $temp[]=['user_id'=>$userID,'permission_id'=>$p];
              $save=UserPermissions::saveUserPermissions($userID,$temp);
              if($save){
                $result['result']=true;
                $result['message']='User Permissions Saved';
              }
            }else{
              $result['message']='No permissions selected.';
            }
          }
          return json_encode($result);
        }
      // }
      // return view('enrollment.blank',['message'=>"You dont have permission."]);
    }

    public function createuser(){
      if(isset($this->permissions['user_create'])){
        return view('user.createuser',$this->data);
      }else{
        return view('enrollment.blank',['message'=>"You dont have permission."]);
      }
    }

    public function addUser(Request $request){
        $rules = [
              'name' => 'required|string|max:15',
              'email' => 'required|string|email|max:80|unique:users',
              'password' => 'required|string|min:6|confirmed'
        ];
        $request->validate($rules);
        $data=['name'=>$request['name'],'email'=>$request['email'],'password'=>Hash::make($request['password']),'verified'=>0];
        $save=User::insert($data);
        if($save){
            $title = 'Success';
            $icon = 'success';
            $res = true;
            $message = 'Successfully Added';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
    }

    public function profile(){
      switch(Auth::user()->user_type){
        case 0: 
          break;
        case 1:
          $employeeID=Auth::user()->conn_id;
          $this->data['info']=Employees::where('EmployeeID','=',$employeeID)->first();
          $dateCreate = date_create($this->data['info']['BirthDate']);
          $this->data['info']['BirthDate'] = date_format($dateCreate, "F m, Y");
          $this->data['email']=Auth::user()->email;
          $this->data['image']=ImageController::getEmployeeImage($employeeID);;
          return view('hris.profile', $this->data);
          break;
        case 2:
          $studentID=Auth::user()->conn_id;
          $student=Students::where('StudentID','=',$studentID)->first();
          $info=StudentInfo::where('id',$student->studentInfoID)->where('firstName','=',$student->FirstName)->first();  
          if($info) {
            $this->data['info']=$info;
            $this->data['image']=ImageController::getTorRequestImage($studentID);;    
          }
          return view('enrollment.view_preenrollee', $this->data);
          break;
        default:
          break;
      }
    }

    public function userNotifications(){
        $userID=Auth::user()->id;
        if($userID==4) $userID=0;
        $this->data['notifications']=$this->getUserNotifications($userID);
        return view('user.notifications',$this->data);
    }

    public function x_getUserNotifications(Request $request){
        $userID=Auth::user()->id;
        if($userID==4) $userID=0;
        $r=$this->getUserNotifications($userID,true);
        if($r) return json_encode(['notifications'=>$r->toArray(),'count'=>$r->count(),'result'=>true]);
        return json_encode(['result'=>false]);
    }

    public function x_notificationMarkRead(Request $request){
        $data=['result'=>false,'message'=>'Unable to process request'];
        $postData=Input::except(['_token']);
        $notificationID=decrypt($postData['eid']);
        if(UserNotifications::where('id','=',$notificationID)->exists()){
            $update=UserNotifications::where('id','=',$notificationID)
                ->update(['status'=>'read']);          
            $data['id']=$notificationID;
            $data['result']=true;
            $data['message']="Message mark as read successfully";
        }else{
            $data['result']=false;
        }
        return json_encode($data);
    }

    public function userMessages(){
        $userID=Auth::user()->id;
        if($userID==4) $userID=0;
        $this->data['messages']=$this->getUserMessages($userID);
        return view('user.messages',$this->data);
    }

    public function checkSession(Request $request){
        $semester = $request->session()->get('semester');
        $from = $request->session()->get('termFrom');
        $to = $request->session()->get('termTo');
        if($semester>0 && $from>0 && $to>0){
          return json_encode(['semester'=>$semester,'semesterLabel'=>$this->setSemesterName($semester),'term'=>$from.'-'.$to,'from'=>$from,'result'=>true]);
        }
        return json_encode(['result'=>false]);
    }
}
