<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Traits\TCommonFunctions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Traits\TPermissions;
use App\User;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers, TPermissions, TCommonFunctions;
    private $data = [];
    private $permissions;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('csrf'); 
        $this->middleware(function ($request, $next) {
            $this->userID = Auth::user()->id;
            $this->permissions = $this->getPermissions($this->data, true);
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users()
    {
        return view('main.users.index',$this->data);
    }

    public function x_users(Request $request)
    {
        $d = $request->input();
        $data = ['draw' => 0, 'recordsFiltered' => 0, 'recordsTotal' => 0, 'data' => []];
        $data['draw'] = $d['draw'];
        $offset = $d['start'];
        $limit = $d['length'];
        // $columns = [0 => 'LastName', 1 => 'CourseCode', 5 => 'EmpLastName', 3 => 'created_by'];
        $columns = [0 => 'email', 1 => 'verified', 2 => 'name',3 => 'name'];
        $orderBy = '';
        $orderType = '';
        if (isset($d['order'][0]['column'])) {
            $orderBy = $columns[$d['order'][0]['column']];
            $orderType = $d['order'][0]['dir'];
        }
        $gradeChanges = [];
        $filter = $d['search']['value'];
        $status = 0;

            $res = User::where(function ($res) use ($filter) {
                if (! empty($filter)) {
                    $res->where('users.name', 'like', '%'.$filter.'%');
                    $res->orWhere('users.email', 'like', '%'.$filter.'%');
                }
            })
            ->skip($offset)
            ->take($limit)
            ->orderBy($orderBy, $orderType);
            $res = $res->get();

            $c = $res->count();
        
            if ($c > 0) {
                $List = [];
                $records = $res->toArray();
                foreach ($records as $r) {
                    $actions = '';
                    $actions = '<a href="'.route('userPermissions', encrypt($r['id'])).'" class="btn btn-inverse-secondary p-2">Permission</a>';
                    $List[] = [
                    '<span class="text-primary font-weight-bold">'.$r['name'].'</span>',
                    ''.$r['email'].'',
                    ''.($r['verified'] == 1?'<div class="text-left mt-2 text-primary"><i class="mdi mdi-check-decagram"></i> Verified</div>':'').'',
                    $actions,
                    ];
                }

                $totals = User::where(function ($totals) use ($filter) {
                    if (! empty($filter)) {
                        $totals->where('users.name', 'like', '%'.$filter.'%');
                        $totals->orWhere('users.email', 'like', '%'.$filter.'%');
                    }
                })
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderBy, $orderType)->count();
                $data['recordsTotal'] = $totals;
                $data['recordsFiltered'] = $totals;

            $data['data'] = $List??false;

            return json_encode($data);
        }
    }
}
