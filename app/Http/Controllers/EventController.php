<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Models\EventModule;
use App\Models\EventDetails;
use App\Models\Member;
use Illuminate\Support\Str;
use \Endroid\QrCode\QrCode as QRID;
use App\Traits\TQRID;
use App\Traits\TCommonFunctions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Traits\TPermissions;
use App\User;
class EventController extends Controller
{
    use AuthenticatesUsers, TPermissions, TCommonFunctions;
    private $data = [];
    private $permissions;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('csrf'); 
        $this->middleware(function ($request, $next) {
            $this->userID = Auth::user()->id;
            $this->user_type = Auth::user()->user_type;
            $this->permissions = $this->getPermissions($this->data, true);
            return $next($request);
        });
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function events()
    {
        return view('main.event.index',$this->data);
    }
    public function event_add()
    {
        return view('main.event.addevent',$this->data);
    }
    public function viewDetails(Request $request)
    {
        $this->data['id'] = $request->segment(3);
        $this->data['code'] = decrypt($request->segment(4));
        $c = EventModule::where('id','=',decrypt($request->segment(3)))->first();
        
        $f = EventDetails::where('event_code','=',$c->event_code)->orderBy('date')->get();
        $h = Member::where('member_code','=',$c->event_code)->get();
        $this->data['root'] = $request->root();
        $this->data['f'] = $f;
        $this->data['c'] = $c;
        $this->data['h'] = $h;
        return view('main.event.viewDetails',$this->data);
    }

    public function fAttendance(Request $request)
    {
        $this->data['id'] = $request->segment(2);
        $this->data['code'] = decrypt($request->segment(3));

        $c = EventModule::where('id','=',decrypt($request->segment(2)))->first();

        $f = EventDetails::where('event_code','=',$c->event_code)->orderBy('date')->get();

        $h = Member::where('member_code','=',$c->event_code)->get();
        // print_r(json_encode($h));die();
        $this->data['f'] = $f;
        $this->data['c'] = $c;
        $this->data['h'] = $h;
        return view('main.event.fattendance',$this->data);
    }

    public function x_editEvent(Request $request){
        $event_id = decrypt($request->id);
        $c = EventModule::where('id','=',$event_id)->first();
        $d = EventDetails::where('event_code','=',$c->event_code)->get();
        $f = Attendance::where('event_code','=',$c->event_code)->count();
        if($f < 1){
            $r = 0;
            foreach ($d as $k) {
                $ds[] = [
                    'date' => $k->date,
                    'in' => $k->in,
                    'out' => $k->out,
                ];
            }
        }else{
            $r = 1;
        }
        $c['ids'] = encrypt($c->id);
        return json_encode([$c,'scheds'=>$ds??[],'deadline'=>date('Y-m-d H:i',strtotime($c->event_deadline)),'startDate'=>date('Y-m-d H:i',strtotime($c->event_startReg)),$r]);
    }

    public function x_enableAttendance(Request $request)
    {
        $a = decrypt($request->a);
        $d = $request->d;
        if($request->b == 'once'){
            $c = $request->c == 1?0:1;
            $data = [
                'enable'=>$c
            ];
        }else{
            // print_r(json_encode($request->b));
            // die();
            $b = ($request->b == NULL || $request->b == 1 || $request->b == '')?0:1;
            if($d == 'in'){
                $data = [
                    'in_stat'=>$b,
                ];
            }else{
                $data = [
                    'out_stat'=>$b,
                ];
            }
        }
        $c = EventDetails::where('id','=',$a)
        ->update($data);
        print_r(json_encode([$a]));
    }

    public function x_SetAsDone(Request $request){
        $event_code = decrypt($request->event_code);
        $title = 'Error';
        $icon = 'error';
        $res = false;
        $message = 'You don\'t have permission to do this action';

        if (isset($this->permissions['event_done'])) {
            $c = EventModule::where('event_code','=',$event_code)->update(['status'=>'done']);
            if($c){
                $title = 'Success';
                $icon = 'success';
                $res = true;
                $message = 'Event successfully set as done';
            }
            return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
        } else {
            $this->data['message'] = 'You don\'t have permission to do this action';

            return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
        }
    }
    public function x_delEvent(Request $request)
    {
        $event_code = decrypt($request->event_code);
        // print_r(json_encode($event_code));die();
        $title = 'Error';
        $icon = 'error';
        $res = false;
        $message = '';

        if (isset($this->permissions['event_delete'])) {
            $c = EventModule::where('event_code','=',$event_code)->delete();
            $f = EventDetails::where('event_code','=',$event_code)->delete();
            if($c || $f){
                $title = 'Success';
                $icon = 'success';
                $res = true;
                $message = 'Successfully Deleted';
            }
            return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
        } else {
            $this->data['message'] = 'You don\'t have permission to do this action';

            return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
        }
    }

    public function x_upcoming(){
        $userID = $this->userID;
        $c = EventDetails::leftjoin('events','events.event_code','=','event_details.event_code')
        ->where('event_details.date','>',date('Y-m-d',strtotime('now')))
        ->where('events.created_by','=',$userID)
        ->limit(4)
        ->orderBy('event_details.date')
        ->get();
        
        $df = '';
        foreach ($c as $k) {
            $d = Member::where('member_code','=',$k->event_code)->count();
            $df .= '<div class="d-block mt-2 h5 shadow-sm p-2 font-weight-bold"><a target="_blank" href="'.route('viewDetails',[encrypt($k->id),encrypt($k->event_code)]).'">'.$k->event_name.'</a> <span class="text-primary d-block h6 font-weight-bold">'.date('M. d, Y',strtotime($k->date)).'</span><span class="text-primary d-block h6 font-weight-normal">'.date('h:i:s A',strtotime($k->in)).' <i class="mdi mdi-arrow-right"></i> '.date('h:i:s A',strtotime($k->out)).' ('.$this->calTime($k->in,$k->out).')</span><span class="h7">'.$d.' <label class="font-weight-normal">Participants</label></span></div>';
        }
        return $df;
    }

    public function x_eventInfo(Request $request)
    {
        $id = decrypt($request->id);
        $c = EventModule::where('id','=',$id)->first();
        $f = EventDetails::where('event_code','=',$c->event_code)->orderBy('date')->get();
        // print_r(json_encode($f));
        // die();
        $ffrom = date('Y-m-d H:i:s');
        $fff = EventDetails::where('event_code','=',$c->event_code)
        ->where('date','=',date('Y-m-d'))
        ->where('in','>',date('H:i:s'))
        ->orderBy('date')->first();
        $datetime = $this->diffTime($ffrom,$fff->in??'');

        $df = '';
        $fgfg = '';
        $length = count($f);
        $bbb = 1;
        foreach ($f as $k){
            if($bbb == $length){
                if (date('Y-m-d',strtotime($ffrom)) > date('Y-m-d',strtotime('+1day',strtotime($k->date .' '.$k->out)))) {
                    EventModule::where('event_code','=',$c->event_code)->update(['status'=>'done']);
                }

                $df .= '<div class="d-block mt-2 h7 shadow-sm">'.date('M. d, Y',strtotime($k->date)).'<span class="text-primary d-block mt-1">'.(($ffrom > ($k->date .' '.$k->out))?'<i class="mdi mdi-check-circle text-success"></i>':'').' '.date('h:i:s A',strtotime($k->in)).' <i class="mdi mdi-arrow-right"></i> '.date('h:i:s A',strtotime($k->out)).' ('.$this->calTime($k->in,$k->out).')</span></div>';

            }else{
                $df .= '<div class="d-block mt-2 h7 shadow-sm p-2">'.date('M. d, Y',strtotime($k->date)).'<span class="text-primary d-block mt-1">'.(($ffrom > ($k->date .' '.$k->out))?'<i class="mdi mdi-check-circle text-success"></i>':'').' '.date('h:i:s A',strtotime($k->in)).' <i class="mdi mdi-arrow-right"></i> '.date('h:i:s A',strtotime($k->out)).' ('.$this->calTime($k->in,$k->out).')</span></div>';
            }
            $bbb++;
        }
        $s = new QRID($c->event_code);
        $d = '<center><div class="mb-3" style="padding: 1em;background: #ffff;border-radius:.3em;width:200px;"><img src="'.$s->writeDataUri().'" style="width:150px;"><br>'.$c->event_code.'</div><button class="btn btn-inverse-primary btn-rounded" onclick="copyToClipboard(\''.$c->event_code.'\')">Copy Link</button>'.($c->status != 'done'?'<button class="btn btn-inverse-success btn-rounded ml-2" onclick="setAsAccomplished(\''.encrypt($c->event_code).'\')">Set As Done</button>':'<span class="text-primary d-block mt-1"><i class="mdi mdi-check-circle"></i> Accomplished</span>').'</center>';
        $b = '';
        if ($c->event_setting == 'online') {
            if ($c->platform == 'zoom') {
                $b .= '<small>zoomID:</small> '.$c->username.'<br>';
                $b .= '<small>Passcode:</small> '.$c->passcode;
            }else{
                $b .= '<small>Google Meet:</small> <a href="'.$c->glink.'" target="_blank">'.$c->glink.'</a>';
            }
        }
        $dfd = date('M. d, Y h:i:s A',strtotime($c->event_deadline));
        $startReg = date('M. d, Y h:i:s A',strtotime($c->event_startReg));
        $s = $b??'';
        $ht = $c->status;
        return json_encode([$c,$d,$s,$df,$dfd,0,$ht,$startReg]);
    }

    public function x_AutoCheckAttendance(){
        $ffrom = date('Y-m-d H:i:s');
        $f = EventModule::leftJoin('event_details','event_details.event_code','=','events.event_code')
        ->where('event_details.date','=',date('Y-m-d'))->get();
        foreach ($f as $c) {
            $fff = EventDetails::where('event_code','=',$c->event_code)
            ->where('date','=',date('Y-m-d'))
            ->where('in','>',date('H:i:s'))
            ->orderBy('date')->first();
            // print_r(json_encode($fff));die();
            if($fff){
                $datetime = $this->diffTime($ffrom,$fff->in??'');
                 //30 minutes
                if($c->autoOn == 1){
                    $gggg = EventDetails::where('id','=',$fff->id)
                    ->update(['in_stat'=>($datetime < 1?0:($datetime <= 30 && $datetime >= 1)?1:0)]);
                }
            }
        }
    }

    public function getStatusAttendance($a,$email,$type){
        $c = Attendance::where('event_id','=',$a)
            ->where('email','=',$email)
            ->first();
        
        if($c){
            if($type == 'in' || $type == 'once'){
                $v =( $c->in != NULL )?date('h:i:s A',strtotime($c->in)):'';
            }else{
                $v = ($c->out != NULL) ?date('h:i:s A',strtotime($c->out)):'';
            }
            return $v;
        }else{
            return false;
        }
    }

    // public function x_register(Request $request)
    // {
    //     $code = $request->segment(2);
    //     $this->data['code'] = $code;

    //     $f = EventDetails::where('event_code','=',$code)->get();

    //     $df = '';
    //     foreach ($f as $k) {
    //         $df .= '<div class="d-block mt-2 h7">'.date('M. d, Y',strtotime($k->date)).'<span class="text-primary d-block mt-1">'.date('h:i:s A',strtotime($k->in)).' <i class="mdi mdi-arrow-right"></i> '.date('h:i:s A',strtotime($k->out)).' ('.$this->calTime($k->in,$k->out).')</span></div>';
    //     }
    //     $this->data['sched'] = $df;
    //     return view('main.event.register',$this->data);
    // }

    public function x_saveEvent(Request $request)
    {
        $title = 'Error';
        $icon = 'Unable to do Action';
        $res = false;
        $message = NULL;

        $eventName = $request->event_name;
        $setting = $request->setting;
        $platform = $request->platform;
        $doe = $request->doe;
        $timein = $request->timein;
        $timeout = $request->timeout;
        $zoomID = $request->zoomID;
        $zoomPass = $request->zoomPass;
        $gmeetlink = $request->gmeetlink;
        $deadline = $request->deadline;
        $dtime = $request->dtime;
        $startReg = $request->startReg;
        $startRegtime = $request->startRegtime;
        $strict = $request->strict;
        $type = $request->type;

        $rules = [
            'event_name' => 'required',
            'setting' => 'required',
        ];
        $request->validate($rules);

        $code = strtoupper(Str::random(3).time().Str::random(3));
        
        $c = new EventModule();
        $c->event_code = $code;
        $c->event_name = $eventName;
        $c->event_setting = $setting;
        $c->platform = $platform;
        $c->username = $zoomID;
        $c->passcode = $zoomPass;
        $c->glink = $gmeetlink;
        $c->strict_late = $strict;
        $c->attendancetype = $type;
        $c->event_deadline = $deadline.' '.$dtime;
        $c->event_startReg = $startReg.' '.$startRegtime;
        
        $this->setCommonFields($c);
        $c->save();

        if($c){
            for($i = 0; $i < count($doe); $i++) {
                if(!empty($doe[$i]) && !empty($timein[$i]) && !empty($timeout[$i])){
                    $data[] = [
                        'event_code'=>$code,
                       'date'=>$doe[$i],
                       'in'=>$timein[$i],
                       'out'=>$timeout[$i],
                   ];
                }else{
                    continue;
                }
            }
            EventDetails::insert($data);
            if($setting == 'online'){
                $list = Member::distinct()->select('email')->where('verified','=','1')->get();
                $details = [
                    'title' => $eventName,
                    'link' => 'event.dev.com/event/'.$code,
                    'btn_label' => 'Register Now',
                    'body' => 'Good day, I sent you an email to inform that we are conducting an event called '.$eventName.'. Register Now',
                ];
                \Mail::to($list)->send(new \App\Mail\SendEmail($details));
            }
            $title = 'Success';
            $icon = 'success';
            $res = true;
            $message = 'Successfully Added';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
    }

    public function x_saveEditEvent(Request $request)
    {
        $title = 'Error';
        $icon = 'Unable to do Action';
        $res = false;
        $message = NULL;

        $eventID = decrypt($request->event_id);
        $eventName = $request->event_name;
        $setting = $request->setting;
        $platform = $request->platform;
        $doe = $request->doe;
        $timein = $request->timein;
        $timeout = $request->timeout;
        $zoomID = $request->zoomID;
        $zoomPass = $request->zoomPass;
        $gmeetlink = $request->gmeetlink;
        $deadline = $request->deadline;
        $dtime = $request->dtime;
        $startReg = $request->startReg;
        $startRegtime = $request->startRegtime;

        $strict = $request->strict;
        $type = $request->type;

        $rules = [
            'event_name' => 'required',
            'setting' => 'required',
        ];
        $request->validate($rules);
        $update = [
            'event_name' => $eventName,
            'event_deadline' => $deadline.' '.$dtime,
            'event_setting' => $setting,
            'platform' => $platform,
            'username' => $zoomID,
            'passcode' => $zoomPass,
            'glink' => $gmeetlink,
            'strict_late' => $strict,
            'autoOn' => 1,
            'attendancetype' => $type ,
            'event_startReg' => $startReg.' '.$startRegtime,
        ];

        $c = EventModule::where('id','=',$eventID)->update($update);
        $cc = EventModule::where('id','=',$eventID)->first();

        if($c){
            $f = Attendance::where('event_code','=',$cc->event_code)->count();
            $data =  [];
            if($f < 1){
                EventDetails::where('event_code','=',$cc->event_code)->delete();
                for($i = 0; $i < count($doe); $i++) {
                    if(!empty($doe[$i]) && !empty($timein[$i]) && !empty($timeout[$i])){
                        $data[] = [
                            'event_code'=>$cc->event_code,
                        'date'=>$doe[$i],
                        'in'=>$timein[$i],
                        'out'=>$timeout[$i],
                    ];
                    }else{
                        continue;
                    }
                }
                EventDetails::insert($data);
            }
            $title = 'Success';
            $icon = 'success';
            $res = true;
            $message = 'Successfully Updated';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
    }

    public function x_members(Request $request){
        $d = $request->input();
        $MemberCode = decrypt($d['MemberCode']);
        $data = ['draw' => 0, 'recordsFiltered' => 0, 'recordsTotal' => 0, 'data' => []];
        $data['draw'] = $d['draw'];
        $offset = $d['start'];
        $limit = $d['length'];
        $columns = [0 => 'member_code', 1 => 'Name', 2 => 'email', 3 => 'Cp',4 => 'Department',5 => 'Course',6 => 'YearSection',7 => 'status'];
        $orderBy = '';
        $orderType = '';
        if (isset($d['order'][0]['column'])) {
            $orderBy = $columns[$d['order'][0]['column']];
            $orderType = $d['order'][0]['dir'];
        }
        $filter = $d['search']['value'];
        $status = 0;

            $res = Member::where('member.member_code','=',$MemberCode)
            ->where(function ($res) use ($filter) {
                if (! empty($filter)) {
                    $res->where('member.Name', 'like', '%'.$filter.'%');
                    $res->orWhere('member.email', 'like', '%'.$filter.'%');
                }
            })
            ->skip($offset)
            ->take($limit)
            ->orderBy($orderBy, $orderType);
            $res = $res->get();
            $c = $res->count();
            if ($c > 0) {
                $List = [];
                $records = $res->toArray();
                foreach ($records as $r) {
                    $s = new QRID($r['code']);
                    $actions = "<button onclick='window.location=\"mailto:".$r['email']."\"' class='btn btn-inverse-primary btn-icon btn-rounded'  data-cooltipz-dir='left' aria-label='Email'>
                            <i class='mdi mdi-email'></i>
                          </button>";
                    $List[] = [
                    '<img src="'.($s->writeDataUri()).'" style="width:90px;">',
                    ''.$r['Name'].'',
                    ''.$r['email'].''.($r['verified'] == 1?'<div class="text-left mt-2 text-primary"><i class="mdi mdi-check-decagram"></i> Verified</div>':'<i class="mdi mdi-close-circle text-danger ml-2"></i>'),
                    ''.$r['Cp'].'',
                    ''.$r['Department'].'',
                    ''.$r['Course'].'',
                    ''.$r['YearSection'].'',
                    $actions,
                    ];
                }

                $totals = Member::where('member.member_code','=',$MemberCode)
                ->where(function ($totals) use ($filter) {
                    if (! empty($filter)) {
                        $totals->where('member.Name', 'like', '%'.$filter.'%');
                        $totals->orWhere('member.email', 'like', '%'.$filter.'%');
                    }
                })
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderBy, $orderType)->count();
                $data['recordsTotal'] = $totals;
                $data['recordsFiltered'] = $totals;

            $data['data'] = $List;

            return json_encode($data);
        }else{
            $data['data'] = [];
            return json_encode($data);
        }
    }   

    public static function diffTime($from,$to)
    {
        // Declare and define two dates
        $date1 = strtotime($to); 
        $date2 = strtotime($from); 

        $diff = abs($date2 - $date1); 

        $years = floor($diff / (365*60*60*24)); 
          
        $months = floor(($diff - $years * 365*60*60*24)
                                       / (30*60*60*24)); 
        $days = floor(($diff - $years * 365*60*60*24 - 
                     $months*30*60*60*24)/ (60*60*24));
          
        $hours = floor(($diff - $years * 365*60*60*24 
               - $months*30*60*60*24 - $days*60*60*24)
                                           / (60*60)); 
          
        $minutes = floor(($diff - $years * 365*60*60*24 
                 - $months*30*60*60*24 - $days*60*60*24 
                                  - $hours*60*60)/ 60); 
          
        $seconds = floor(($diff - $years * 365*60*60*24 
                 - $months*30*60*60*24 - $days*60*60*24
                        - $hours*60*60 - $minutes*60)); 

        return $minutes;
    }
    public static function calTime($from,$to)
    {
        // Declare and define two dates
        $date1 = strtotime($to); 
        $date2 = strtotime($from); 

        $diff = abs($date2 - $date1); 
          
        $years = floor($diff / (365*60*60*24)); 
          
        $months = floor(($diff - $years * 365*60*60*24)
                                       / (30*60*60*24)); 
        $days = floor(($diff - $years * 365*60*60*24 - 
                     $months*30*60*60*24)/ (60*60*24));
          
        $hours = floor(($diff - $years * 365*60*60*24 
               - $months*30*60*60*24 - $days*60*60*24)
                                           / (60*60)); 
          
        $minutes = floor(($diff - $years * 365*60*60*24 
                 - $months*30*60*60*24 - $days*60*60*24 
                                  - $hours*60*60)/ 60); 
          
        $seconds = floor(($diff - $years * 365*60*60*24 
                 - $months*30*60*60*24 - $days*60*60*24
                        - $hours*60*60 - $minutes*60)); 
          
        if($days > 1){
            return $days.'d';
        }elseif($hours >= 1){
            return $hours.'h'.($minutes == 0?'':' '.$minutes.'m').($seconds == 0?'':' '.$seconds.'s ');
        }
        elseif($minutes >= 1){
            return $minutes.'m '.($seconds == 0?'':$seconds.'s ');
        }
        elseif($minutes < 1 && $seconds <= 60){
            if($minutes < 1 && $seconds <= 40){
                return 'Just now';
            }else{
                return ($seconds == 0?'':$seconds.'s ');
            }
        }
        // return ($hours == 0?'':$hours.'h ').($minutes == 0?'':$minutes.'m ').($seconds == 0?'':$seconds.'s ');
    }

    public function x_events(Request $request){
        $d = $request->input();
        $data = ['draw' => 0, 'recordsFiltered' => 0, 'recordsTotal' => 0, 'data' => []];
        $data['draw'] = $d['draw'];
        $offset = $d['start'];
        $limit = $d['length'];
        // $columns = [0 => 'LastName', 1 => 'CourseCode', 5 => 'EmpLastName', 3 => 'created_by'];
        $columns = [0 => 'event_name', 1 => 'event_code', 2 => 'created_at', 3 => 'event_setting',4 => 'platform', 5 => 'username'];
        $orderBy = '';
        $orderType = '';
        if (isset($d['order'][0]['column'])) {
            $orderBy = $columns[$d['order'][0]['column']];
            $orderType = $d['order'][0]['dir'];
        }
        $gradeChanges = [];
        $filter = $d['search']['value'];
        $status = 0;
        $userID = $this->userID;
        $userType = $this->user_type;
            if($userType == 1){
                $res = EventModule::select('*','events.id AS event_id')->leftjoin('users','users.id','=','events.created_by')
                ->where(function ($res) use ($filter) {
                    if (! empty($filter)) {
                        $res->where('events.event_name', 'like', '%'.$filter.'%');
                        $res->orWhere('events.event_code', 'like', '%'.$filter.'%');
                    }
                })
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderBy, $orderType);
            }else{
                $res = EventModule::select('*','events.id AS event_id')->leftjoin('users','users.id','=','events.created_by')
                ->where(function ($res) use ($filter) {
                    if (! empty($filter)) {
                        $res->where('events.event_name', 'like', '%'.$filter.'%');
                        $res->orWhere('events.event_code', 'like', '%'.$filter.'%');
                    }
                })
                ->where('events.created_by','=',$userID)
                ->skip($offset)
                ->take($limit)
                ->orderBy($orderBy, $orderType);
            }

            $res = $res->get();

            $c = $res->count();
            
            if ($c > 0) {
                $List = [];
                $records = $res->toArray();
                foreach ($records as $r) {
                    $f = EventDetails::where('event_code','=',$r['event_code'])->orderBy('date')->get();
                    $df = '';
                    foreach ($f as $k) {
                        $df .= '<div class="d-block mt-2 h7">'.date('M. d, Y',strtotime($k->date)).'<span class="text-primary d-block mt-1">'.date('h:i:s A',strtotime($k->in)).' <i class="mdi mdi-arrow-right"></i> '.date('h:i:s A',strtotime($k->out)).' ('.$this->calTime($k->in,$k->out).')</span></div>';
                    }
                    $v = Member::where('member_code','=',$r['event_code'])->count();
                    $b = '';
                    $qr = new QRID($r['event_code']);
                    $actions = '';
                    $qwer = '';
                    if (isset($this->permissions['event_edit']) && $r['status'] != 'done') {
                        // $actions .= '<button class="btn btn-inverse-secondary p-2 m-1">Edit</button>';
                    }
                    if (isset($this->permissions['event_delete']) && $r['status'] != 'done' && ($v == 0)) {
                        if($userID == $r['created_by']){
                            $actions .= '<button class="btn btn-inverse-danger p-2 m-1" onclick="delEvent(\''.encrypt($r['event_code']).'\')">Delete</button>';

                        }
                    }
                    $i = '';
                    if($r['status'] == 'done'){
                        $i = "<span class='d-block text-success mt-2'><i class='mdi mdi-check-circle'></i> Accomplished</span>";
                    }

                    if($userID != $r['created_by']){
                        $qwer = '<a href="#" class="text-primary font-weight-bold">'.$r['event_name'].'</a>'.$i.'<br><small>Created by: <b>'.$r['name'].'</b></small>';
                    }else{
                        $qwer = '<a target="_blank" href="'.route('viewDetails',[encrypt($r['event_id']),encrypt($r['event_code'])]).'" class="text-primary font-weight-bold">'.$r['event_name'].'</a>'.$i.'<br><small>Created by: <b>'.$r['name'].'</b></small>';
                    }
                    if ($r['event_setting'] == 'online') {
                        if ($r['platform'] == 'zoom') {
                            $b .= 'zoomID: '.$r['username'].'<br>';
                            $b .= 'Passcode: '.$r['passcode'];
                        }else{
                            $b .= '<a href="'.$r['glink'].'" target="_blank">'.$r['glink'].'</a>';
                        }
                    }
                    
            
                    $List[] = [
                    '<img src="'.($qr->writeDataUri()).'" style="width:70px">',
                    $qwer,
                    ''.$df.'',
                    '<span class="cusbadge">'.$r['event_setting'].($r['platform']==''?'':' ['.$r['platform'].']').'</span><br><span class="text-primary h7">'.$b.'</span>',
                    '<span class="cusbadge"><span class="text-primary">'.$v.'</span><small> Participants</small></span>',
                    $actions,
                    ];
                }

                if($userType == 1){
                    $totals = EventModule::where(function ($totals) use ($filter) {
                        if (! empty($filter)) {
                            $totals->where('events.event_name', 'like', '%'.$filter.'%');
                            $totals->orWhere('events.event_code', 'like', '%'.$filter.'%');
                        }
                    })
                    ->skip($offset)
                    ->take($limit)
                    ->orderBy($orderBy, $orderType)->count();
                }else{
                    $totals = EventModule::where(function ($totals) use ($filter) {
                        if (! empty($filter)) {
                            $totals->where('events.event_name', 'like', '%'.$filter.'%');
                            $totals->orWhere('events.event_code', 'like', '%'.$filter.'%');
                        }
                    })
                    ->where('events.created_by','=',$userID)
                    ->skip($offset)
                    ->take($limit)
                    ->orderBy($orderBy, $orderType)->count();
                }
                $data['recordsTotal'] = $totals;
                $data['recordsFiltered'] = $totals;

            $data['data'] = $List??false;

            return json_encode($data);
        }else{
            $data['data'] = [];
            return json_encode($data);
        }
    }    
}
