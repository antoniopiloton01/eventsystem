<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\EventModule;
use App\Models\EventDetails;
use App\Models\Attendance;
use Illuminate\Support\Facades\Hash;
use App\Traits\TCommonFunctions;
use Laravel\Ui\Presets\React;
use \Endroid\QrCode\QrCode as QRID;
use App\Traits\TQRID;
use Mockery;
use App\User;
use Mockery\Undefined;

use function GuzzleHttp\json_decode;

class PublicEventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use TCommonFunctions;
    public function __construct()
    {
        $this->middleware('csrf');
    }

    private $data = [];
    private $permissions;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function x_register(Request $request)
    {
        $code = $request->segment(2);
        $checkStatus = EventModule::leftJoin('event_details', 'event_details.event_code', '=', 'events.event_code')
            ->where('event_details.event_code', '=', ($code))
            ->where('events.status', '=', 'done')
            ->exists();
        if ($checkStatus) {
            $this->data['color'] = 'text-success';
            $this->data['header'] = 'ACCOMPLISHED';
            $this->data['desc'] = 'This event is already set as accomplished/done';
            return view('success', $this->data);
        }

        $c = EventModule::where('event_code', '=', $code)->first();
        $this->data['code'] = $code;
        $this->data['data'] = $c;
        $f = EventDetails::where('event_code', '=', $code)->orderby('date')->get();
        $this->data['sched'] = $f;
        return view('main.event.register', $this->data);
    }

    public function register_user(){
        return view('auth.register', $this->data);
    }

    public function reset_password(){
        return view('auth.passwords.email', $this->data);
    }

    public function verify(Request $request){
        $email = decrypt($request->segment(2));
        $code = $request->segment(3);
        $user = User::where('email','=',$email)
        ->where('code','=',$code)
        ->update(['verified'=>1]);
        return view('verified', $this->data);
    }
   
    public function toverify(Request $request){
        $email = $request->segment(2);
        $this->data['email'] = $email;
        return view('auth.verify', $this->data);
    }

    // public function resend(Request $request){
    //     return view('auth.verify', $this->data);
    // }

    public function x_attendance(Request $request)
    {
        $type = encrypt($request->segment(3));
        $id = ($request->segment(4));
        $checkStatus = EventModule::leftJoin('event_details', 'event_details.event_code', '=', 'events.event_code')
            ->where('event_details.id', '=', decrypt($id))
            ->where('events.status', '=', 'done')
            ->exists();
        if ($checkStatus) {
            $this->data['color'] = 'text-success';
            $this->data['header'] = 'ACCOMPLISHED';
            $this->data['desc'] = 'This event is already set as accomplished/done';
            return view('success', $this->data);
        }
        $this->data['id'] = $id;
        $this->data['type'] = $type;
        return view('main.event.attendance', $this->data);
    }

    public function add_user(Request $request){
        $rules = [
              'name' => 'required|string|max:50',
              'email' => 'required|string|email|max:80|unique:users',
              'password' => 'required|string|min:6|confirmed'
        ];
        $request->validate($rules);
        $code = strtoupper(Str::random(7));
        $em = $request['email'];
        $c = explode('@',$em);
        if($c[1] != 'usa.edu.ph'){
            $title = 'WARNING';
            $icon = 'error';
            $res = false;
            $message = 'Your email is not valid in University of Agustin. Please use USA Institutional Email';
            return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title,'email'=>$links??'']);
        }
        $data = [
            'name'=>$request['name'],
            'email'=>$request['email'],
            'password'=>Hash::make($request['password']),
            'verified'=>0,
            'code' => $code
        ];

        $permission = array(1,2,3,4,7,8,9);
        
        $save=User::insert($data);
        if($save){
            $link = $request->root().'/verify/'.encrypt($request['email']).'/'.$code;
            $links = $request->root().'/toverify/'.encrypt($request['email']);
            $details = [
                'title' => 'CONFIRMATION!!',
                'link' => $link,
                'btn_label' => 'Verify Email',
                'body' => 'Good day '.$request['name'].', click the button to verify your account',
            ];
            \Mail::to($request['email'])->send(new \App\Mail\SendEmail($details));
            $title = 'Success';
            $icon = 'success';
            $res = true;
            $message = 'Successfully Added';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title,'email'=>$links??'']);
    }

    public function x_savef2fAttendance(Request $request)
    {
        $title = 'Error';
        $message = 'Unable to do Action';
        $res = false;
        $icon = 'error';
        $rules = [
            'mode' => 'required',
            'sched' => 'required'
        ];
        $request->validate($rules);

        $code = explode('@', $request->code);
        if(is_numeric($code[0])){
            $qrcode = ($code[0] / 10) / 10;
        }else{
            $message = 'Broken QR Code';
            $icon = "text-danger";
            return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
        }
        $type = $request->mode;
        $id = $request->sched;
        $event_id = $request->event_id;
        $MemberCode = decrypt($request->MemberCode_);
        $getData = Member::where('id', '=', $qrcode)->first();

        if($id == 'undefined' || $type == 'undefined'){
            $message = 'Select schedule first';
            $icon = "text-danger";
            return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
        }
        // print_r(json_encode([$getData,$id]));
        // die();
        if ($getData) {
            $email = $getData->email;
            $check = Attendance::where('email', '=', $email)
                ->where('event_id', '=', $id)
                ->first();
            if($check){
                if($type == 'in' || $type == 'once'){
                    if($check->in == NULL){
                        if($check->out != NULL){
                            $c = Attendance::where('email', '=', $email)
                            ->where('event_id', '=', $id)
                            ->update(['in' => date('H:i:s')]);
                        }else{
                            $c = new Attendance();
                            $c->event_id = $id;
                            $c->email = $email;
                            $c->event_code = $MemberCode;
                            $c->in = date('H:i:s');
                            $this->setCommonFields($c);
                            $c->save();
                        }
                        
                        if ($c) {
                            $title = 'Success';
                            $icon = 'text-success';
                            $res = true;
                            $message = '<h1><i class="mdi mdi-check-circle"></i></h1>'.$getData->Name;
                        }
                    }else{
                        $title = 'Success';
                        $icon = 'text-success';
                        $res = true;
                        $message = '<h1><i class="mdi mdi-check-circle"></i></h1>'.$getData->Name;
                    }
                }else{
                    if($check->in != NULL){
                        $c = Attendance::where('email', '=', $email)
                        ->where('event_id', '=', $id)
                        ->update(['out' => date('H:i:s')]);
                    }else{
                        $c = new Attendance();
                        $c->event_id = $id;
                        $c->email = $email;
                        $c->event_code = $MemberCode;
                        $c->out = date('H:i:s');
                        $this->setCommonFields($c);
                        $c->save();
                    }
                    
                    if ($c) {
                        $title = 'Success';
                        $icon = 'text-success';
                        $res = true;
                        $message = '<h1><i class="mdi mdi-check-circle"></i></h1>'.$getData->Name;
                    }
                }
            }else{
                // $message = 'SULOD';
                if($id != NULL || !empty($id)){
                    $c = new Attendance();
                    $c->event_id = $id;
                    $c->email = $email;
                    $c->event_code = $MemberCode;
                    if($type == 'out'){
                        $c->out = date('H:i:s');
                    }else{
                        $c->in = date('H:i:s');
                    }
                    $this->setCommonFields($c);
                    $c->save();

                    if($c){
                        $icon = 'text-success';
                        $message = '<i class="mdi mdi-check-circle"></i>'.$getData->Name;
                    }
                }
            }
        }else{
            $title = 'Error';
            $icon = 'text-danger';
            $res = false;
            $message = 'It seems like your QR Code does not match on our records';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
    }

    public function x_saveAttendance(Request $request)
    {
        $title = 'Error';
        $message = 'Unable to do Action';
        $res = false;
        $icon = 'error';

        $id = decrypt($request->id);
        $type = decrypt($request->type);
        $email = $request->email;

        $rules = [
            'email' => 'required',
        ];
        $request->validate($rules);
        // $checkifExist = EventDetails::where('event_details.id','=',$id)->exists();
        // if(!$checkifExist){
        //     $title = 'Error';
        //     $icon = 'error';
        //     $res = false;
        //     $message = 'It seems like the link is wrong';
        //     return json_encode(['result' => $res, 'message' => $message, 'icon'=>$icon, 'title'=>$title]);
        // }

        $checkifEmailRegistered = Member::leftJoin('event_details', 'event_details.event_code', '=', 'member.member_code')
            ->leftJoin('events', 'events.event_code', '=', 'event_details.event_code')
            ->where('event_details.id', '=', $id)
            ->where('member.email', '=', $email)
            // ->where('member.verified','=',1)
            ->first();

        if ($type == 'once') {
            $checkifEnable = EventDetails::where('event_details.id', '=', $id)->where('enable', '=', 1)->first();
        } else {
            if ($type == 'in') {
                $checkifEnable = EventDetails::where('event_details.id', '=', $id)->where('in_stat', '=', 1)->first();
            } else {
                $checkifEnable = EventDetails::where('event_details.id', '=', $id)->where('out_stat', '=', 1)->first();
            }
        }
        // print_r(json_encode($strict));
        // die();

        if ($checkifEnable) {
            $checkifStrictLate = EventModule::leftJoin('event_details', 'event_details.event_code', '=', 'events.event_code')
                ->where('event_details.id', '=', $id)
                ->where('events.strict_late', '=', 1)
                ->exists();
            $qwe = $checkifEnable->date . ' ' . ($checkifEnable->out);
            $now = date('Y-m-d H:i:s', strtotime('now'));
            // print_r(json_encode([strtotime($now),strtotime($qwe)]));
            // die();
            $strict = false;
            if ($checkifStrictLate) {
                if (strtotime($now) > strtotime($qwe)) {
                    $strict = ($type == 'out') ? true : false;
                } else {
                    $strict = ($type == 'out') ? false : true;
                }
            }
            if (!$strict) {
                if ($type == 'in' || $type == 'once') {
                    $checkifEmailCpExist = Attendance::where('email', '=', $email)
                        ->where('event_id', '=', $id)
                        ->where('in', '!=', NULL)
                        ->first();
                } else {
                    $checkifEmailCpExist = Attendance::where('email', '=', $email)
                        ->where('event_id', '=', $id)
                        ->where('out', '!=', NULL)
                        ->first();
                }

                if ($checkifEmailRegistered) {

                    if ($checkifEmailRegistered->verified != 1) {
                        $title = 'Error';
                        $icon = 'error';
                        $res = false;
                        $message = 'Your email is not yet verified. We sent you already an email for the confimation after your registration';
                        return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
                    }


                    if (!$checkifEmailCpExist || $checkifEmailCpExist == NULL) {
                        $checkifInIsFilled = Attendance::where('email', '=', $email)
                            ->where('event_id', '=', $id)
                            ->where('in', '!=', NULL)
                            ->first();

                        $checkifOutIsFilled = Attendance::where('email', '=', $email)
                            ->where('event_id', '=', $id)
                            ->where('out', '!=', NULL)
                            ->first();

                        if ($type == 'in' || $type == 'once') {
                            if ($checkifOutIsFilled) {
                                $c = Attendance::where('email', '=', $email)
                                    ->where('event_id', '=', $id)
                                    ->update(['in' => date('H:i:s')]);
                            } else {
                                $c = new Attendance();
                                $c->event_id = $id;
                                $c->email = $email;
                                $c->event_code = $checkifEmailRegistered->event_code;
                                $c->in = date('H:i:s');
                                $this->setCommonFields($c);
                                $c->save();
                            }
                        } else {
                            if ($checkifInIsFilled) {
                                $c = Attendance::where('email', '=', $email)
                                    ->where('event_id', '=', $id)
                                    ->update(['out' => date('H:i:s')]);
                            } else {
                                $c = new Attendance();
                                $c->event_id = $id;
                                $c->email = $email;
                                $c->event_code = $checkifEmailRegistered->event_code;
                                $c->out = date('H:i:s');
                                $this->setCommonFields($c);
                                $c->save();
                            }
                        }

                        if ($c) {
                            $title = 'Success';
                            $icon = 'success';
                            $res = true;
                            $message = 'Thank you!';
                            // $details = [
                            //     'title' => $checkifEmailRegistered->event_name,
                            //     'link' => '',
                            //     'body' => 'Attendance ['.$type.'] Status: SUCCESS     Time: '.date('Y-m-d H:i:s'),
                            // ];
                            // \Mail::to($email)->send(new \App\Mail\SendEmail($details));
                        }
                    } else {
                        $title = 'Error';
                        $icon = 'error';
                        $res = false;
                        $message = $checkifEmailCpExist->email . ' you already take an attendance on this session';
                    }
                } else {
                    $title = 'Error';
                    $icon = 'error';
                    $res = false;
                    $message = 'You are not registered on this event';
                }
            } else {
                $title = 'Error';
                $icon = 'error';
                $res = false;
                $message = "The form is no longer accepting attendance for late. Try contacting the owner of the form if you think that this is a mistake.";
            }
        } else {
            $title = 'Error';
            $icon = 'error';
            $res = false;
            $message = 'The form is not yet enabled to take an attendance';
        }
        return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
    }

    public function x_confirm(Request $request)
    {
        $code = $request->segment(3);
        $c = Member::where('verifycode', '=', $code)->update(['verified' => '1']);
        $cc = Member::where('verifycode', '=', $code)->first();
        $d = EventModule::leftJoin('member', 'member.member_code', 'events.event_code')
            ->where('member.verifycode', '=', $code)
            ->where('events.event_setting', '=', 'online')
            ->first();

        $b = '';
        if ($c) {
            $url = $request->root();
            $links = (($cc->id * 10) * 10) . '@' . $cc->code; //1500@12345
            $qrlink = new QRID($links);
            if ($d && $d->event_setting == 'online') {
                if ($d->platform == 'zoom') {
                    $b .= 'zoomID:' . $d->username . '      ';
                    $b .= 'Passcode: ' . $d->passcode;
                } else {
                    $b .= 'Google Meet:' . $d->glink;
                }
                $details = [
                    'title' => $d->event_name,
                    'link' => '',
                    'body' => $b,
                ];
                \Mail::to($d->email)->send(new \App\Mail\SendEmail($details));
            }

            $this->data['QR'] = $qrlink->writeDataUri();
            return view('verified', $this->data);
        } else {
            $this->data['message'] = "It seems like your code is not correct";
            return view('notfound', $this->data);
        }
    }

    public function x_saveMember(Request $request)
    {
        $title = 'Error';
        $icon = 'Unable to do Action';
        $res = false;
        $message = NULL;

        $code = decrypt($request->code);
        $email = $request->email;
        $name = $request->name;
        $cp = $request->cp;
        $department = $request->department;
        $course = $request->course;
        $yearsection = $request->yearsection;

        $rules = [
            'email' => 'required',
            'cp' => 'required',
            'department' => 'required',
            'course' => 'required',
            'yearsection' => 'required'
        ];
        $request->validate($rules);

        $getEventDetails = EventModule::where('event_code', '=', $code)
            ->first();
        $StartReg = strtotime($getEventDetails->event_startReg);
        $Deadline = strtotime($getEventDetails->event_deadline);
        $now = strtotime('now');
        if ($now > $Deadline) {
            $title = 'ERROR';
            $icon = 'error';
            $res = false;
            $message = 'This form is no longer accepting applicant. <br>Try contacting the owner of the form if you think that this is a mistake.';
            return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
        }else if ($now < $StartReg) {
            $title = 'ERROR';
            $icon = 'error';
            $res = false;
            $message = "This form is not accepting applicant for this time. <br>Try again this ".date('F d, Y h:i:s A',strtotime($getEventDetails->event_startReg));
            return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
        }else{
            $checkifEmailCpExist = Member::where('email', '=', $email)
            ->where('member_code', '=', $code)
            ->exists();

            if (!$checkifEmailCpExist) {
                $vcode = rand(100000, 999999);
                $c = new Member();
                $c->code = strtoupper(Str::random(3)) . time();
                $c->member_code = $code;
                $c->Name = $name;
                $c->email = $email;
                $c->Cp = $cp;
                $c->verifycode = $vcode;
                $c->Department = $department;
                $c->Course = $course;
                $c->YearSection = $yearsection;
                $c->save();

                $v = Member::where('email', '=', $email)
                    ->where('member_code', '=', $code)
                    ->first();

                if ($v) {
                    $url = $request->root();
                    $link = $url . '/confirm/email/' . $vcode;
                    $details = [
                        'title' => 'CONFIRMATION!!!',
                        'link' => $link,
                        'btn_label' => 'Confirm Email',
                        'body' => 'Good Day ' . $name . ', here is your confirmation code: ' . $vcode,
                    ];
                    \Mail::to($email)->send(new \App\Mail\SendEmail($details));
                }

                if ($c) {
                    $title = 'Success';
                    $icon = 'success';
                    $res = true;
                    $message = 'Please check your email to verify your account';
                }
            } else {
                $title = 'Error';
                $icon = 'error';
                $res = false;
                $message = 'Email already registered on this event';
            }
            return json_encode(['result' => $res, 'message' => $message, 'icon' => $icon, 'title' => $title]);
        }
    }
}
