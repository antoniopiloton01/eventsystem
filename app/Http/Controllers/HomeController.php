<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Traits\TPermissions;
use App\Traits\TCommonFunctions;
use App\Models\EventModule;
use App\Models\EventDetails;
use App\Models\Member;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers, TPermissions, TCommonFunctions;
    private $data = [];
    private $permissions;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('csrf'); 
        $this->middleware(function ($request, $next) {
            $this->userID = Auth::user()->id;
            $this->userType = Auth::user()->user_type;
            $this->permissions = $this->getPermissions($this->data, true);
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userType = $this->userType;
        if ($userType == 1) {
            $events = EventModule::where('archived','=',0)->count();
            $member = Member::distinct('email')->where('archived','=',0)->count();
            $accomplished_event = EventModule::where('status','=','done')->count();
            $this->data['events'] = $events;
            $this->data['members'] = $member;
            $this->data['accomplished_event'] = $accomplished_event;
            return view('home',$this->data);
        }else{
            return redirect()->route('events');
        }
    }    
}
