<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $table ='roles';

    public static function getEmployeeRole($employeeID):string{
      $r=Role::select('roles.name')
        ->leftJoin('employee_roles', 'roles.id','=','employee_roles.role_id')
        ->where('employee_roles.employee_id','=',$employeeID)
        // ->limit(1)
        ->first();
      if($r){
        return $r->name;
      }else{
        return 'none';
      }
    }
}