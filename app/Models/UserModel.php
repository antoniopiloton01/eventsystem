<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class UserModel extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'StudentID', 'StudentYear', 'StudentID2', 'StudentCode', 'FirstName', 'MiddleName', 'LastName', 'NameExtension', 'BirthDate', 'Gender', 'CivilStatus', 'Citizenship', 'StudentTypeID', 'CurriculumID',
        'Address', 'studentInfoID', 'PhoneNumber', 'YearLevel', 'Email',
        'hregion', 'hprovince', 'hcity', 'hbarangay',
        'created_by', 'updated_by', 'status', 'created_at', 'updated_at', 'archived',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $table = 'students';
}
