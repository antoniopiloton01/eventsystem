<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class RolePermissions extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'permission_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $table ='role_permissions';

    public static function getRolePermissions($roleID){
      $rp=RolePermissions::select('permissions.id','permissions.code','permissions.details','permissions.area')
        ->leftJoin('permissions','permissions.id','=','role_permissions.permission_id')
        ->where('role_permissions.role_id','=',$roleID)
        ->where('permissions.dev','=',0)
        ->orderBy('permissions.area')
        ->orderBy('permissions.code')
        ->get();
      if($rp){
        return $rp->toArray();
      }
      return false;
    }

    public static function saveRolePermissions($roleID,$data){
      $delete=RolePermissions::where('role_id','=',$roleID)->delete();
      $save=RolePermissions::insert($data);
      if($save){
        return true;
      }
      return false;
    }


    public static function saveRolePermission($roleID,$data){
      $save=RolePermissions::insert($data);
      if($save){
        return true;
      }
      return false;
    }
}
