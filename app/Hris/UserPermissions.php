<?php

namespace App\Hris;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserPermissions extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'permission_id', 'exclude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $table ='user_permissions';

    public function users(){
        return $this->belongsToMany('App\Users');
    }

    public function permissions(){
        return $this->belongsToMany('App\Permissions');
    }

    public static function getUserPermissions($userID){
      $up=UserPermissions::select('permissions.id','permissions.code','permissions.details','permissions.area')
        ->leftJoin('permissions','permissions.id','=','user_permissions.permission_id')
        ->where('user_permissions.user_id','=',$userID)
        ->where('permissions.dev','=',0)
        ->orderBy('permissions.area')
        ->orderBy('permissions.code')
        ->get();
      if($up){
        return $up->toArray();
      }
      return false;
    }

    public static function saveUserPermissions($userID,$data){
      $delete=UserPermissions::where('user_id','=',$userID)->delete();
      $save=UserPermissions::insert($data);
      if($save){
        return true;
      }
      return false;
    }


    public static function saveUserPermission($userID,$data){
      $save=UserPermissions::insert($data);
      if($save){
        return true;
      }
      return false;
    }
}
