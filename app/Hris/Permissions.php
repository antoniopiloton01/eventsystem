<?php

namespace App\Hris;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Permissions extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code','details'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];

    protected $table ='permissions';

    public static function getAllPermissions(){
        $res=Permissions::select('*')->where('dev','=',0)->orderBy('details')->get();
        return count($res)>0?$res:false;
    }

    public function users(){
        return $this->belongsToMany('App\User','User');
    }

    public static function getPermissionByCode($permission){
        $res=Permissions::where('code','=',$permission)->first();
        return $res?$res:false;
    }
}
