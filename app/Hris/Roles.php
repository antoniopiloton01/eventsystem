<?php

namespace App\Hris;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Roles extends Authenticatable
{
    use Notifiable;

    const ACCOUNTANT = 7;
    const LIBRARIAN = 6;
    const DSA = 8;
    const REGISTRAR = 9;
    const SSG = 10;
    const DEAN = 12;
    const PROGRAMCHAIR=14;
    const COLLEGESEC=4;
    const CD = 21;
    const VPACAD = 22;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code','name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $table ='roles';
}
